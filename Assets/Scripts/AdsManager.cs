﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if(Advertisement.isSupported) {
            Advertisement.Initialize("1088341", false);
        }        
    }

    public void showFullscreenAd() {
        if(PlayerPrefs.GetInt("adFree", 0) != 1 && PlayerPrefs.GetInt("adsRequest", 1) >= 5) {
            Advertisement.Show(null, new ShowOptions
            {
                resultCallback = result =>
                {
                    Debug.Log("Ad result: " + result);
                }
            });

            PlayerPrefs.SetInt("adsRequest", 0);
        } else {
            PlayerPrefs.SetInt("adsRequest", PlayerPrefs.GetInt("adsRequest", 1) + 1);
        }


        Debug.Log("Ad request #" + PlayerPrefs.GetInt("adsRequest", 1));
        PlayerPrefs.Save();
    }

     public void ShowRewardedAd(){
        if (Advertisement.IsReady("rewardedVideo")) {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }

        Debug.Log("clicked");
    }

    private void HandleShowResult(ShowResult result){
        switch (result) {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                Currency.Add(100);
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        GameObject.Find("Currency").GetComponent<Counter>().RequestUpdate();
    }
    
}
