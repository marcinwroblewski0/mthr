﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class JourneyMenu : MonoBehaviour {

    public GameObject[] galaxies;

    public void GoToStoryteller() {
        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("storyteller");
    }

    void Start() {
        
        int journeyLevel = PlayerPrefs.GetInt("journeyLevel", 0);
        if(journeyLevel == 0) {
            GoToStoryteller();
            return;
        }

        GameObject map = GameObject.Find("Map");

        galaxies = new GameObject[map.transform.childCount];

        for(int i = 0; i < map.transform.childCount; i++) {
            galaxies[i] = map.transform.GetChild(i).gameObject;
        }

        Color playerColor = Ship.shipColors[PlayerPrefs.GetInt("Player1Color", -1)];
        int currentGalaxy = PlayerPrefs.GetInt("currentGalaxy", 1);

        for(int i = 0; i < journeyLevel - 1; i++) {
            galaxies[i].GetComponent<SpriteRenderer>().color = playerColor;
        } 

        Debug.Log((PlayerPrefs.GetInt("journeyLevel", 0)));

        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();
    }
}
