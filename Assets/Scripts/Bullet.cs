﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	private string parentName;
    private GameStates GS;

	public void setParentName(string name){
		parentName = name;
	}

	public string getParentName(){
		return parentName;
	}

	void Start () {
		Destroy (gameObject, 4);
        GS = GameObject.Find("_GM").GetComponent<GameStates>();
    }

    void FixedUpdate() {
        if (GS.getGameState() == false) {
            Destroy(gameObject);
        }
    }
}
