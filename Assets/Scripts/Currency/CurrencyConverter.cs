﻿using UnityEngine;
using System.Collections;

public class CurrencyConverter : MonoBehaviour {

	public static int ForRoundTime(int time) {
        return (int)Mathf.Ceil(time * 0.1f);
    }

    public static int ForScreenTouches(int touches) {
        return (int)Mathf.Ceil(touches * 0.05f);
    }

    public static int ForBullets(int bullets) {
        return (int)Mathf.Ceil(bullets * 0.01f);
    }

    public static int ForTurnovers(int turnovers) {
        return (int)Mathf.Ceil(turnovers * 0.5f);
    }

    public static int ForPowerups(int powerups) {
        return (int)Mathf.Ceil(powerups * 0.25f);
    }

    public static int ForMeteoritesStolen(int stolenMeteorites) {
        return (int)Mathf.Ceil(stolenMeteorites * 0.1f);
    }

    public static int For(int time, int touches, int bullets, int turnovers, int powerups, int stolenMeteorites) {
        return ForRoundTime(time) + ForScreenTouches(touches) + ForBullets(bullets) + ForTurnovers(turnovers) +
            ForPowerups(powerups) + ForMeteoritesStolen(stolenMeteorites);
    }
}
