﻿using UnityEngine;
using System.Collections;

public class Currency : MonoBehaviour {

    public static string CURRENCY_KEY = "currency";

	public static void Add(int howMany) {
        Debug.Log("Added " + howMany);

        PlayerPrefs.SetInt(CURRENCY_KEY, PlayerPrefs.GetInt(CURRENCY_KEY, 0) + howMany);
        PlayerPrefs.Save();

        //UNDONE connecting to GPG
    }

    public static void Take(int howMany) {
        PlayerPrefs.SetInt(CURRENCY_KEY, PlayerPrefs.GetInt(CURRENCY_KEY, 0) - howMany);
        PlayerPrefs.Save();
    }

    public static int Available() {
        return PlayerPrefs.GetInt(CURRENCY_KEY, 0);
    }

    public static bool HaveMoreThan(int howMany) {
        return Currency.Available() >= howMany;
    }
}
