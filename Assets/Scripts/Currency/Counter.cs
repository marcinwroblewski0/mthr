﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Counter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = Currency.Available().ToString();
	}

    public void RequestUpdate() {
        GetComponent<Text>().text = Currency.Available().ToString();
    }
}
