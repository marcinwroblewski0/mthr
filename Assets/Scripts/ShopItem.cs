﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopItem : MonoBehaviour {

    public bool adShowed = false;

    void Start() {

        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();

        GameObject camo = GameObject.Find("Camo");
        GameObject puzl = GameObject.Find("Puzl");
        GameObject hnmb = GameObject.Find("Hnmb");
        GameObject fade = GameObject.Find("Fade");

        Color playerColor = Ship.shipColors[PlayerPrefs.GetInt("Player1Color", Random.Range(0, 10))];

        if(Skins.BoughtSkins() == 0) {
            

            puzl.GetComponent<Image>().color = new Color(0, 0, 0);
            puzl.GetComponent<Button>().enabled = false;

            hnmb.GetComponent<Image>().color = new Color(0, 0, 0);
            hnmb.GetComponent<Button>().enabled = false;

            fade.GetComponent<Image>().color = new Color(0, 0, 0);
            fade.GetComponent<Button>().enabled = false;
        } else if(Skins.BoughtSkins() == 1) {

            camo.GetComponent<Image>().color = playerColor;

            hnmb.GetComponent<Image>().color = new Color(0, 0, 0);
            hnmb.GetComponent<Button>().enabled = false;

            fade.GetComponent<Image>().color = new Color(0, 0, 0);
            fade.GetComponent<Button>().enabled = false;
        } else if(Skins.BoughtSkins() == 2) {

            camo.GetComponent<Image>().color = playerColor;
            puzl.GetComponent<Image>().color = playerColor;

            fade.GetComponent<Image>().color = new Color(0, 0, 0);
            fade.GetComponent<Button>().enabled = false;
        } else if (Skins.BoughtSkins() == 3) {
            camo.GetComponent<Image>().color = playerColor;
            puzl.GetComponent<Image>().color = playerColor;
            hnmb.GetComponent<Image>().color = playerColor;
        } else if( Skins.BoughtSkins() == 3) {
            camo.GetComponent<Image>().color = playerColor;
            puzl.GetComponent<Image>().color = playerColor;
            hnmb.GetComponent<Image>().color = playerColor;
            fade.GetComponent<Image>().color = playerColor;
        }

        if(!adShowed) {
            GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().showFullscreenAd();
            adShowed = true;
        }
    }

	public void Buy(int id) {
        if (Skins.BoughtSkins() >= id) {
            Skins.SetCurrentSkin(id);
            return;
        }

        int cost = 0;

        switch (id) {
            case 1: cost = 300; break;
            case 2: cost = 500; break;
            case 3: cost = 800; break;
            case 4: cost = 1100; break;
        }    

        if(Currency.HaveMoreThan(cost)) {
            Currency.Take(cost);
            GameObject.Find("Currency").GetComponent<Counter>().RequestUpdate();
            Skins.SetCurrentSkin(id);
            Skins.SetBoughtSkins(id);
            Start();
        }
    }
}
