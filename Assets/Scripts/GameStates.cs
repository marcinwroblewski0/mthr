﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameStates : MonoBehaviour {

	public bool canPlay;

	// Use this for initialization
	void Start () {
		canPlay = false;
	}

	void Update(){
		if (SceneManager.GetActiveScene().buildIndex == 0) {
			Destroy(gameObject);
		}
	}

	public void setGameState(bool canPlay){
		this.canPlay = canPlay;
	}

	public bool getGameState(){
		return canPlay;
	}
}
