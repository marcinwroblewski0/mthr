﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ShipConfigurationManager : MonoBehaviour {

    public int forPlayer;
    public InputField shipName;
    public int colorId;

    private Color[] shipColors = Ship.shipColors;

    void Start() {
        Image ship = GameObject.Find("Ship").GetComponent<Image>();

        if(forPlayer == 1) {
            if(!PlayerPrefs.GetString("nick", "").Equals("")) {
                shipName.text = PlayerPrefs.GetString("nick", "...");

                
                colorId = PlayerPrefs.GetInt("Player1Color");
                ship.color = Ship.shipColors[colorId];
                shipName.textComponent.color = Ship.shipColors[colorId];
                ship.sprite = GetComponent<Skins>().CurrentSkinSprite();
            } else {
                colorId = Random.Range(0, shipColors.Length);
                ship.color = Ship.shipColors[colorId];
                shipName.textComponent.color = Ship.shipColors[colorId];
            }
        } else {
            if(!PlayerPrefs.GetString("nick" + forPlayer, "").Equals("")) {
                shipName.text = PlayerPrefs.GetString("nick" + forPlayer, "...");
            }

            GameObject takenColor = GameObject.Find("Image" + PlayerPrefs.GetInt("Player1Color", -1));
            takenColor.GetComponent<Image>().color = Color.black;
            takenColor.GetComponent<Button>().enabled = false;

            
            int random = Random.Range(0, shipColors.Length);
            if(forPlayer == 2) {
                while (random == PlayerPrefs.GetInt("Player1Color", -1)) {
                    random = Random.Range(0, shipColors.Length);
                }
            }

            ship.color = shipColors[random];
            shipName.textComponent.color = shipColors[random];
            
            GameObject.Find("Done").GetComponent<ShipConfigurationManager>().colorId = random;
        }

        
        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();
    }

    public void SaveConfiguration() {

        if (shipName.text.Equals("")) {
            shipName.text = "Titanic";
        }

        if (shipName.text.Length > 8) {
            shipName.text = shipName.text.Substring(0, 8);
        }

        if(forPlayer == 1) {
            PlayerPrefs.SetString("nick", shipName.text);
        } else {
            PlayerPrefs.SetString("nick" + forPlayer, shipName.text);
        }

        PlayerPrefs.SetInt("Player" + forPlayer + "Color", colorId);

        //CHEAT
        if(shipName.text.Equals("$$!!$!")) Currency.Add(1000);
        if (shipName.text.Contains("!")) StartCoroutine(Cheats.lookFor(shipName.text)); 
        //CHEAT

        Debug.Log("Ship " + forPlayer + " name: " + shipName.text + ", color id: " + colorId);

        if (PlayerPrefs.GetInt("tutorial_done", 0) == 0) {
            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("tutorial");
            return;
        }

        PlayerPrefs.Save();

        if(forPlayer == 1) {
            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("menu");
        } else {
            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("multiL");
        }

        
    }

}
