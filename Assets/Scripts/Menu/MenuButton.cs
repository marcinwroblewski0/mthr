﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuButton : MonoBehaviour {

    private IEnumerator pulse() {
        while(true) {
            for(int i=0; i <= 25; i++) {
               
                GetComponentInChildren<Text>().transform.localScale = new Vector2(GetComponentInChildren<Text>().transform.localScale.x + i/1000f, GetComponentInChildren<Text>().transform.localScale.y + i/1000f);
                yield return new WaitForSeconds(0.03f);
            }

            for(int i=0; i <= 25; i++) {
                GetComponentInChildren<Text>().transform.localScale = new Vector2(GetComponentInChildren<Text>().transform.localScale.x - i/1000f, GetComponentInChildren<Text>().transform.localScale.y - i/1000f);;
                yield return new WaitForSeconds(0.03f);
            }
        }
    }

    IEnumerator fadeOutScene() {
        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(button);
    }

	public string button;

    void Start() {
        GetComponentInChildren<Text>();
        if(button.Equals("journey")) {
            StartCoroutine(pulse());
        }

        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();
    }

	public void GoTo(){
        StartCoroutine(fadeOutScene());
	}
}
