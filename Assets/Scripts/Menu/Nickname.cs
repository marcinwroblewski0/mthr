﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Nickname : MonoBehaviour {

	private InputField nicknameField;

	void Start(){
		nicknameField = gameObject.GetComponent<InputField>();

		if(gameObject.name.Contains("2")){
			nicknameField.text = PlayerPrefs.GetString ("nick2", "Player2");
		}else{
			nicknameField.text = PlayerPrefs.GetString ("nick", "Player1");
		}
	}

	public void setNewNickname(){
        if (nicknameField.text.Length > 8) {
            nicknameField.text = nicknameField.text.Substring(0, 8);
        }

		if(gameObject.name.Contains("2")){
            PlayerPrefs.SetString("nick2", nicknameField.text);
		}else{
			PlayerPrefs.SetString("nick", nicknameField.text);
		}
	}
}
