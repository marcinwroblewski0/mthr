﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorChooseButton : MonoBehaviour {

    public int id;
   

    private Image ship;

    // Use this for initialization
    void Start () {
        GetComponent<Image>().color = Ship.shipColors[id];
        ship = GameObject.Find("Ship").GetComponent<Image>();
     
    }

    public void PaintShip() {
        ship.color = Ship.shipColors[id];
        GameObject.Find("Ship Name").GetComponent<InputField>().textComponent.color = Ship.shipColors[id];
        GameObject.Find("Done").GetComponent<ShipConfigurationManager>().colorId = id;
    }
}
