﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class RecordManager : MonoBehaviour {

    public Text timer;
    private int meteoritesCount;
    private int[] results;
    private RoundTimeManager rtm;
    private MeteoriteGenerator mg;
    private PowerupsManager pm;
    private GameStates gs;
    private Weapon p1Weapon, p2Weapon;
    private Ship p1Ship;
    public int stolenFromP2toP1, stolenFromP1toP2;
    private Move p1Move, p2Move;
    private TransitionMask mask;
    private int screenTouches;

    private float roundTime, startTime;
    private bool countingTime;

    IEnumerator countdown() {
        gs.setGameState(false);
        for(float i=3f; i > 0f; i-=0.1f) {
            
            
            if (Random.Range(1, 30) == 16 && i%1f == 0) {
                timer.text = "Survive";
                yield return new WaitForSeconds(0.2f);
            } else if (Random.Range(1, 12) == 10  && i%1f == 0){
                timer.text = "Fight";
                yield return new WaitForSeconds(0.2f);
            } else if (Random.Range(1, 100) == 97  && i%1f == 0){
                timer.text = "Run";
                yield return new WaitForSeconds(0.3f);
            } else {
                timer.text = i.ToString("0.0");
            }
            

            yield return new WaitForSeconds(0.1f);
        }
        gs.setGameState(true);
        countingTime = true;
        roundTime = 0;
        startTime = Time.time;
    }

	void Start () {
        PlayerPrefs.SetString("BotName", "Molly");
        PlayerPrefs.Save();

        rtm = GetComponent<RoundTimeManager>();
        mg = GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>();
        pm = GetComponent<PowerupsManager>();
        gs = GetComponent<GameStates>();
        p1Weapon = GameObject.FindGameObjectWithTag("Player1").GetComponentInChildren<Weapon>();
        p2Weapon = GameObject.FindGameObjectWithTag("Player2").GetComponentInChildren<Weapon>();
        p1Ship = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>();
        p1Move = GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>();
        p2Move = GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>();

        mask = GameObject.Find("TransitionMask").GetComponent<TransitionMask>();
        mask.fadeInScene();

        startNewRound();
        StartCoroutine(countdown());
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)){
            screenTouches++;
        }
    }

	void FixedUpdate () {
        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        CountTime();

        if (isMollyWinning()) playerLost("Molly won");
    }

    public void startNewRound(){
        screenTouches = 0;

        mg.rotateDirection = Random.Range(-2, 2);

        p1Weapon.bulletSpeed = 30f;
        p1Weapon.shootRate = 0.47f;
        p1Weapon.bulletSpeed = 30f;
        p1Weapon.shootRate = 0.47f;

        p1Move.turnovers = 0;
        p2Move.turnovers = 0;

        stolenFromP1toP2 = 0;
        stolenFromP2toP1 = 0;

        p1Weapon.resetBulletCount();
        p1Ship.powerupsUsed = 0;

        mg.setupNewRound();

        GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>().setStartPosition();
        GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>().setStartPosition();

        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        mg.setupNewRound();
        
    }


    public void playerLost(string why) {
        gs.setGameState(false);

        PlayerPrefs.SetInt("lastSingleTouches", screenTouches);
        PlayerPrefs.SetInt("lastSingleStolen", stolenFromP2toP1);
        PlayerPrefs.SetFloat("lastSingleTurnovers", p1Move.turnovers);
        PlayerPrefs.SetInt("lastSinglePowerups", p1Ship.powerupsUsed);
        PlayerPrefs.SetInt("lastSingleBullets", p1Weapon.getBulletCount());
        PlayerPrefs.SetFloat("lastSingleTime", roundTime);
        PlayerPrefs.Save();

        mask.fadeOutScene("singleSummary");
    }

    private bool isMollyWinning() {
        return
            (results[0] < results[1] && (float)results[1] / (float)meteoritesCount * 100f > 50 && gs.getGameState());
    }

    private void CountTime() {
        if(countingTime) {
            if(gs.getGameState()) {
                roundTime = Time.time - startTime;
                if (roundTime < 100) {
                    timer.text = roundTime.ToString("0.0");
                } else {
                    timer.text = roundTime.ToString("0");
                }
            }
        }
    }
}
