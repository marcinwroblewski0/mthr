﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundTimeManager : MonoBehaviour {

    public float roundTime, startTime, timeLeft, endTime;
    public Text timeLeftLabel;
    private bool isRoundRequestSend;
    private GameStates gs;
    private PointsManager pm;
    private AchievementsManager am;
    private JourneyManager jm;
    private bool alertFinished;

    IEnumerator alert() {
        Color normal = Color.white;
        Color alert = Color.red;

        alertFinished = false;
        for (int i = 0; i < 25; i++){
            timeLeftLabel.transform.localScale = new Vector2(
                timeLeftLabel.transform.localScale.x + 0.03f,
                timeLeftLabel.transform.localScale.y + 0.03f
                );
            timeLeftLabel.color = new Color(
                (float)i/25,
                timeLeftLabel.color.g - ((float)i / 25),
                timeLeftLabel.color.b - ((float)i / 25), 
                1f);
            yield return new WaitForSeconds(0.02f);
        }

        for (int i = 0; i < 25; i++){
            timeLeftLabel.transform.localScale = new Vector2(
                timeLeftLabel.transform.localScale.x - 0.03f,
                timeLeftLabel.transform.localScale.y - 0.03f
                
                );
            timeLeftLabel.color = new Color(
                timeLeftLabel.color.r + ((float)i / 25), 
                (float)i / 25, 
                (float)i / 25, 
                1f);
            yield return new WaitForSeconds(0.02f);
        }

        alertFinished = true;
    }

    // Use this for initialization
    void Start () {
        gs = GameObject.Find("_GM").GetComponent<GameStates>();
        pm = GameObject.Find("_GM").GetComponent<PointsManager>();
        am = GameObject.Find("_GM").GetComponent<AchievementsManager>();
        jm = GameObject.Find("_GM").GetComponent<JourneyManager>();
        SetTimeRules(20f);

        alertFinished = true;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if(timeLeft > 0 && gs.getGameState()) {
            timeLeft = endTime - Time.time;
            timeLeftLabel.text = Mathf.Ceil(timeLeft).ToString();
        }

        if (timeLeft <= 5 && alertFinished) {
            StartCoroutine(alert());
        }

        if (timeLeft <= 0 && gs.getGameState() && !isRoundRequestSend) {
            isRoundRequestSend = true;
            if (pm != null) {
                pm.requestRoundEnd();
            } else if (am != null) {
                am.playerLost("Time's up");
            } else if (jm != null) {
                jm.requestRoundEnd();
            }
            SetTimeRules(15f);
        }   
    }

    public void SetTimeRules(float roundTime) {
        this.roundTime = roundTime + 8f; //round panel displayed
        startTime = Time.time;
        timeLeft = (roundTime + startTime) - Time.time;
        Debug.Log("Time left :" + timeLeft);
        endTime = (roundTime + startTime);
        isRoundRequestSend = false;
    }
}
