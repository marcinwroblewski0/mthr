﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Ship : MonoBehaviour
{

    public Color shipColor;
    public string playerName;
    public string player;
    public TrailRenderer trail;
    public int powerupsUsed;
    public int collidedWithOpponent;


    public static Color[] shipColors = new Color[]{
        new Color(255/255f,  66/255f,   83/255f, 1f),  
        new Color(255/255f, 149/255f,    0/255f, 1f),  //red
        new Color(255/255f, 219/255f,   75/255f, 1f),   
         
        new Color( 88/255f, 86/255f,  214/255f, 1f),   
        new Color(  0/255f, 122/255f, 255/255f, 1f),  //blue
        new Color( 85/255f, 236/255f, 206/255f, 1f),   

        new Color( 93/255f, 213/255f,  44/255f, 1f),    
        new Color(120/255f, 170/255f,  80/255f, 1f),   //green
        new Color(157/255f, 230/255f, 125/255f, 1f), 

		new Color(193/255f,  69/255f, 250/255f, 1f),     
        new Color(238/255f,  77/255f, 185/255f, 1f),    //purple
        new Color(225/255f, 174/255f, 238/255f, 1f)       
    };

    public static Color steveColor = new Color(0/255f, 137/255f, 123/255f, 1f);
    public static Color natalieColor = new Color(198/255f, 255/255f, 0/255f, 1f);
    public static Color johnColor = new Color(255/255f, 160/255f, 0/255f, 1f);
    public static Color mollyColor = new Color(159/255f, 151/255f, 75/255f, 1f);


    public int getPowerupsUsed()
    {
        return powerupsUsed;
    }

    public void setShipColor(Color color)
    {
        shipColor = color;
        gameObject.GetComponent<SpriteRenderer>().color = shipColor;
    }

    public void setPlayerName(string name)
    {
        playerName = name;
        gameObject.name = playerName;
    }

    public Color getShipColor()
    {
        return shipColor;
    }

    public string getPlayerName()
    {
        return playerName;
    }

    public void showTrail(bool shouldShow)
    {
        if (shouldShow)
            trail.enabled = true;
        else
            trail.enabled = false;
    }

    public void disableCollider() {
        GetComponent<BoxCollider2D>().enabled = false;
    }

    // Use this for initialization
    void Start()
    {

        player = gameObject.tag.Substring(gameObject.tag.Length - 1, 1);

        if(int.Parse(player) == 1) {
            GetComponent<SpriteRenderer>().sprite = GetComponent<Skins>().CurrentSkinSprite();
        }

        Color color = mollyColor;

        if (PlayerPrefs.GetInt("Player" + player + "Color", -1) != -1){
            color = shipColors[PlayerPrefs.GetInt("Player" + player + "Color", -1)];
            
        } else {
            if (SceneManager.GetActiveScene().name.Equals("multiL")) SceneManager.LoadScene(5);
            
        }
       
        setPlayerName(gameObject.name);


        if (SceneManager.GetActiveScene().buildIndex != 2 && int.Parse(player) == 2) {
            
            setPlayerName(PlayerPrefs.GetString("BotName", "Molly"));
            
            switch(playerName) {
                case "Steve":
                    color = steveColor;
                    PlayerPrefs.SetFloat("difficulty", 1f);
                    break;
                case "Natalie":
                    color = natalieColor;
                    PlayerPrefs.SetFloat("difficulty", 2f);
                    break;
                case "John":
                    color = johnColor;
                    PlayerPrefs.SetFloat("difficulty", 3f);
                    break;
                case "Molly":
                    color = mollyColor;
                    PlayerPrefs.SetFloat("difficulty", 4f);
                    break;
            }

            PlayerPrefs.Save();
            Debug.Log(PlayerPrefs.GetString("BotName", "Molly"));
        }

        setShipColor(color);

        trail = gameObject.GetComponentInChildren<TrailRenderer>();
        trail.enabled = false;
        trail.material.color = color;
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag.Contains("Player"))
            collidedWithOpponent++;
        
    }
}
