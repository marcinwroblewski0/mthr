﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class Cheats : MonoBehaviour {

    public static IEnumerator lookFor(string text) {

        Debug.Log("Connecting to promo server...");
        WWW target = new WWW("http://marcinwroblewski.16mb.com/projekty/meteorite/bonus/promo.json");
        yield return target;

        var receivedData = JSON.Parse(target.text);

        if(receivedData["for"] != null) {
            string data = receivedData["for"];

            if (data.Equals(text) && PlayerPrefs.GetInt("promo", 0) != 1) {
                Debug.Log("Promo code worked");

                PlayerPrefs.SetInt("adFree", 1);
                PlayerPrefs.SetInt("promo", 1);
                Currency.Add(350);
                PlayerPrefs.Save();
            }
            else Debug.Log("Bad promo code or promo already used");

        } else {
            Debug.LogError("Connection error");
        }

        
    }

}
