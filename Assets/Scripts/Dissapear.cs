﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Dissapear : MonoBehaviour {

    private IEnumerator dissapear() {
        GetComponent<Image>().color = new Color(0, 0, 0, 0);
        yield return new WaitForSeconds(5f);

        for(int i=0; i<255; i++) {
            GetComponent<Image>().color = new Color(1, 1, 1, (float)i/255f);
            yield return new WaitForSeconds(0.001f);
        }

        for(int i=255; i>0; i--) {
            GetComponent<Image>().color = new Color(1, 1, 1, (float)i/255f);
            yield return new WaitForSeconds(0.001f);
        }
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(dissapear());
	}

}
