﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	private Transform[] BGLayers;
    public Sprite darkGalaxyBg1;
    public Sprite darkGalaxyBg2;
    public Sprite darkGalaxyBg3;
    public Sprite sweetGalaxyBg1;
    public Sprite sweetGalaxyBg2;
    public Sprite sweetGalaxyBg3;

    IEnumerator fadeInLayers() {

        for(int i = BGLayers.Length - 1; i >= 0; i--) {
            for (int j = 0; j <= 255/2; j++) {
                BGLayers[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, j/255f*2f);
                yield return new WaitForEndOfFrame();
            }
        }
    }

	// Use this for initialization
	void Start () {
		BGLayers = new Transform[gameObject.transform.childCount];
		for (int i=0; i<BGLayers.Length; i++) {
			BGLayers[i] = gameObject.transform.GetChild(i);
		}
        SetGalaxyBackground();
        StartCoroutine(fadeInLayers());
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("graphics", 1) == 1) {
			for (int i = 1; i<BGLayers.Length + 1; i++) {
				BGLayers [i - 1].Rotate (new Vector3(0, 0, (Random.Range (0.4f, 2f) * Time.deltaTime)) / (float)i);
			}
		}
	}

    void SetGalaxyBackground() {
        switch (PlayerPrefs.GetInt("currentGalaxy", 1)) {
            case 1: break;
            case 2:
                BGLayers[0].GetComponent<SpriteRenderer>().sprite = darkGalaxyBg1;
                BGLayers[1].GetComponent<SpriteRenderer>().sprite = darkGalaxyBg2;
                BGLayers[2].GetComponent<SpriteRenderer>().sprite = darkGalaxyBg3;
                break;
            case 3:
                BGLayers[0].GetComponent<SpriteRenderer>().sprite = sweetGalaxyBg1;
                BGLayers[1].GetComponent<SpriteRenderer>().sprite = sweetGalaxyBg2;
                BGLayers[2].GetComponent<SpriteRenderer>().sprite = sweetGalaxyBg3;
                break;
        }
    }
}
