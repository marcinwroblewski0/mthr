using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUI : MonoBehaviour {

	public Text P01Pts, P02Pts;

	void OnGUI(){
		int[] results = GameObject.Find ("MeteoriteParent").GetComponent<MeteoriteGenerator> ().countAllMeteorites();
		float meteoritesCount = (results [0] + results [1] + results [2]);

		float P01Points = results [0] / meteoritesCount * 100;
		float P02Points = results [1] / meteoritesCount * 100;
		float clearPoints = results [2] / meteoritesCount * 100;

		P01Pts.text = string.Format("{0:N1}", P01Points);
		P02Pts.text = string.Format("{0:N1}", P02Points);

		P01Pts.fontSize = (int)P01Points + 35;
		P02Pts.fontSize = (int)P02Points + 35;

		P01Pts.color = GameObject.FindGameObjectWithTag ("Player1").GetComponent<SpriteRenderer> ().color;
		P02Pts.color = GameObject.FindGameObjectWithTag ("Player2").GetComponent<SpriteRenderer> ().color;
	}
}
