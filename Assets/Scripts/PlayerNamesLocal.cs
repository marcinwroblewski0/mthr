﻿using UnityEngine;
using System.Collections;

public class PlayerNamesLocal : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.FindGameObjectWithTag ("Player1").GetComponent<Ship> ().setPlayerName (PlayerPrefs.GetString ("nick", "Player1"));

		if (GameObject.FindGameObjectWithTag ("Player2").name.Equals ("AI")) {
			GameObject.FindGameObjectWithTag ("Player2").GetComponent<Ship> ().setPlayerName ("Molly");
		} else {
			GameObject.FindGameObjectWithTag ("Player2").GetComponent<Ship> ().setPlayerName (PlayerPrefs.GetString ("nick2", "Player2"));
		}
	}
}
