using UnityEngine;
using UnityEngine.UI;

public class ColorChooseSlider : MonoBehaviour {

    public string player;
    private Image textfield;
    private Slider colorslider;

    private Color[] shipColors = new Color[]{
            new Color(137/255f, 140/255f, 144/255f, 1f),    //grey
            new Color(255/255f, 205/255f, 2/255f, 1f),      //yellow
            new Color(255/255f, 94/255f, 58/255f, 1f),      //orange
            new Color(255/255f, 59/255f, 48/255f, 1f),      //red
            new Color(90/255f, 200/255f, 251/255f, 1f),     //cyan 
            new Color(26/255f, 165/255f, 253/255f, 1f),     //blue
            new Color(88/255f, 86/255f, 214/255f, 1f),      //purple
            new Color(135/255f, 252/255f, 112/255f, 1f),    //light green
            new Color(34/255f, 181/255f, 65/255f, 1f),     //green
			new Color(90/255f, 212/255f, 39/255f, 1f),      //dark green
            new Color(200/255f, 110/255f, 223/255f, 1f),    //pink
            new Color(255/255f, 45/255f, 85/255f, 1f)       //strong pink
        };

    void Start () {
        textfield = GameObject.Find("NicknameField" + player).GetComponent<Image>();
        colorslider = GetComponent<Slider>();

        colorslider.minValue = 0;
        colorslider.maxValue = (shipColors.Length - 1);

        if (PlayerPrefs.GetInt("Player" + player + "Color", -1) != -1) {
            textfield.color = shipColors[PlayerPrefs.GetInt("Player" + player + "Color", -1)];
            colorslider.value = PlayerPrefs.GetInt("Player" + player + "Color", -1);
        } else {
            int random = Random.Range(0, shipColors.Length);
            textfield.color = shipColors[Random.Range(0, random)];
            colorslider.value = random;
        }
	}

    public void ColorChanged() {
        textfield.color = shipColors[(int)GetComponent<Slider>().value];

        PlayerPrefs.SetInt("Player" + player + "Color", (int)colorslider.value);
    }
}
