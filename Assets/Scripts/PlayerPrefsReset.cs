﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsReset : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetString("0.22reset", "nope").Equals("nope")) {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetString("0.22reset", "yep");
        }

	}
}
