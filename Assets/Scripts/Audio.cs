﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour {

    public AudioClip normal;
    public AudioClip dark;
    public AudioClip sweet;

	void Awake() {
        DontDestroyOnLoad(gameObject);

        switch (PlayerPrefs.GetInt("currentGalaxy", 1)) {
            case 1: GetComponent<AudioSource>().clip = normal; break;
            case 2: GetComponent<AudioSource>().clip = dark; break;
            case 3: GetComponent<AudioSource>().clip = sweet; break;
        }
        GetComponent<AudioSource>().Play();

    }
}
