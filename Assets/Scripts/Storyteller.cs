﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Storyteller : MonoBehaviour {

    public Text story;
	int slides, currentSlide;

    bool isStorytelling = false;


    string[] galaxiesStoryStrings = {
        "Genesis is a place where all began. \nRules just created and everyone had to learn them.",
        "Now we're heading to Steve's galaxy. It was annexed by other species.",
        "This is John's galaxy, he's so lazy, he don't even notice that someone is watching him.",
        "This is Natalie's galaxy, it looks cute, but do not be absent-minded. Natalie is clever, and ready for defense.",
        "And finally, Molly's galaxy, she got it back from an evil creature called Mio. She learned a lot and she still learning to be ready for defense."};

    string[] levelsStoryStrings = {
        "But the rules was simple. \nTake over all meteories! ...or at least the greater part.",
        "Steve: <i>What's that? Looks like colorful circles...</i> \nFind out what's going on.",
        "Steve: <i>Oh! There are more of them!</i>",
        "Steve: <i>What are they?</i>",
        "Steve: <i>Have we seen all of them?</i>",
        "Steve: <i>Maybe it's a right moment to introduce myself. \nI'm Steve.</i>",
        "Steve: <i>Have you met Natalie? \nShe's a bit better than me.</i>",
        "Steve: <i>Now you'll play with John. \nHe's very clever but still beatable.</i>",
        "Steve: <i>And this is Molly. \nShe's... \nGood luck</i>",
        "Steve: <i>Oh, this galaxy used to be mine! Help me get it back</i>", //First galaxy fight (Steve's)
        "Steve: <i>Don't let him win. He's strong but together we'll win!</i>",
        "Steve: <i>He's like a louse</i>",
        "Steve: <i>Don't play with him, I want my galaxy back!</i>",
        "Steve: <i>Just like that, one more and galaxy will be mine again! \nUhm, I mean ours! Ours...</i>", 
        "Steve: <i>Now, we will take his place. Let's see if John had prepared.</i>", //Second galaxy
        "Steve: <i>An eye for an eye and a tooth for a tooth</i>",
        "Steve: <i>He is laughing! This is unacceptable</i>",
        "Steve: <i>He laughs best who laughs last</i>",
        "Steve: <i>This galaxy is different. Beware... </i>", //Third
        "Steve: <i>Natalie is tricky</i>", 
        "Steve: <i>But we have to be smarter than she</i>",
        "Steve: <i>May hay while the sun shines</i>",
        "Steve: <i>And final blow. Goodbye Natalie</i>",
        "Steve: <i>Time for Molly, be careful</i>",  //Fourth
        "Steve: <i>I told you that she is cruel</i>",
        "Steve: <i>Watch your back</i>",
        "Steve: <i>Slowly but firmly</i>",
        "Steve: <i>We can use power ups again, now, finish her</i>",
        "Steve: <i>Now your're in the most wanted ship in the universe. And I'll be a hero if I beat you</i>",
        "Steve: <i>Are you having fun? By the time</i>",
        "Steve: <i>Whole universe will be mine! Because of you</i>",
        "Steve: <i>You're ridiculous, go away</i>",
        "Steve: <i>You'll never defeat me, ever</i>"
        };

    IEnumerator showStoryPanel() {

        Debug.Log("Story settings: galaxy #" + PlayerPrefs.GetInt("currentGalaxy") + ", level #" + PlayerPrefs.GetInt("journeyLevel", 1));

        isStorytelling = true;
        string[] storyStrings = null;

        if (PlayerPrefs.GetInt("journeyLevel", 1) > 14) {
            Debug.Log("End of levels");
            storyStrings = new string[1];
            storyStrings[0] = "Steve: <i>Calm down, we're on the way to the next galaxy</i>";
            
        } else if(!PlayerPrefs.HasKey("galaxy" + PlayerPrefs.GetInt("currentGalaxy") + "intro")) {
            Debug.Log("Need to tell about galaxy");
            storyStrings = new string[2];

            storyStrings[0] = galaxiesStoryStrings[PlayerPrefs.GetInt("currentGalaxy", 1) - 1];
            storyStrings[1] = levelsStoryStrings[PlayerPrefs.GetInt("journeyLevel", 1) - 1];
            PlayerPrefs.SetString("galaxy" + PlayerPrefs.GetInt("currentGalaxy") + "intro", "showed");
            PlayerPrefs.Save();
        } else {
            Debug.Log("No need to tell about galaxy");
            storyStrings = new string[1];

            try {
                storyStrings[0] = levelsStoryStrings[PlayerPrefs.GetInt("journeyLevel", 1) - 1];
            } catch {
                 storyStrings[0] = "Steve: <i>Calm down, we're on the way to the next galaxy</i>";
            }
        }
        

        slides = storyStrings.Length;

        for(int i = 0; i < slides; i++) {
            story.text = storyStrings[i];

            for (int j = 0; j <= 255; j++) {
                story.color = new Color(1, 1, 1, j/255f);
                yield return new WaitForSeconds(0.00005f);
            }

            Debug.Log("Wait " + (storyStrings[0].Length * 0.025f) + " to user read story");
            yield return new WaitForSeconds(storyStrings[0].Length * 0.025f);
            

            for (int j = 255; j >= 0; j--) {
                story.color = new Color(1, 1, 1, j/255f);
                yield return new WaitForSeconds(0.00005f);
            }
            
        }
        if (PlayerPrefs.GetInt("journeyLevel", 1) > 14)
            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("menu");
        else 
            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("journeyScene");

    }

    public void setupStory() {
        switch (PlayerPrefs.GetInt("journeyLevel", 1)) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                PlayerPrefs.SetInt("currentGalaxy", 1);
                PlayerPrefs.SetString("BotName", "Steve");
                break;
            case 7:
                PlayerPrefs.SetInt("currentGalaxy", 1);
                PlayerPrefs.SetString("BotName", "Natalie");
                break;
            case 8:
                PlayerPrefs.SetInt("currentGalaxy", 1);
                PlayerPrefs.SetString("BotName", "John");
                break;
            case 9:
                PlayerPrefs.SetInt("currentGalaxy", 1);
                PlayerPrefs.SetString("BotName", "Molly");
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                PlayerPrefs.SetInt("currentGalaxy", 2);
                PlayerPrefs.SetString("BotName", "John");
                break;


        }
        
        PlayerPrefs.Save();
    }

    void Start() {
        setupStory();
        StartCoroutine(showStoryPanel());
        LookForAchievements();
    }

    void LookForAchievements() {
        if(PlayerPrefs.GetInt("journeyLevel", 1) >= 9) {
            GPGAchievements.ReportAchievement(GPGAchievements.GOING_TO_AN_ADVENTURE);
        } 
    }
}
