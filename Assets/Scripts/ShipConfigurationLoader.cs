﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ShipConfigurationLoader : MonoBehaviour {

    public Text shipName;
    public Image ship;
    public Image skin;


    IEnumerator fadeOut() {
        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(4);
    }

    void Start () {

        shipName = GameObject.Find("ShipName").GetComponent<Text>();
        ship = GameObject.Find("Ship").GetComponent<Image>();

        if (PlayerPrefs.GetInt("Player1Color", -1) == -1){
            SceneManager.LoadScene(4);
        } else {
            shipName.text = PlayerPrefs.GetString("nick", "Titanic");
            shipName.color = Ship.shipColors[PlayerPrefs.GetInt("Player1Color", 0)];
            ship.color =  Ship.shipColors[PlayerPrefs.GetInt("Player1Color", 0)];
        }


        ship.sprite = GetComponent<Skins>().CurrentSkinSprite();
    }

    public void EditShipConfiguration() {
        StartCoroutine(fadeOut());
    }
}
