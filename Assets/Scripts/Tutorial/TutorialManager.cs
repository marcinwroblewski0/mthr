﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialManager : MonoBehaviour {

    public Button left, right;
    public GameObject ship;
    public Transform meteoriteParent;
    public Text tutorialText;
    public int tutorialStep;

    bool pulseInProgress = false;

    public IEnumerator pulse(Button b) {
        pulseInProgress = true;
        for(float i=0; i<=1f; i+=.05f) {
            b.image.color = new Color(1f, 1f, 1f, i);
            yield return new WaitForSeconds(0.03f);
        }

        for(float i=1f; i>=0f; i-=.05f) {
            b.image.color = new Color(1f, 1f, 1f, i);
            yield return new WaitForSeconds(0.03f);
        }

        b.image.color = new Color(0, 0, 0, 0);
        pulseInProgress = false;
    }

    public IEnumerator goLeft() {
        tutorialText.text = "Thanks";
        for(int i=0; i<50; i++) {
            ship.GetComponent<Move>().steerRight();
            yield return new WaitForSeconds(.02f);
        }
        tutorialStep = 1;
        tutorialText.text = "Now tap to steer right";
    }

    public IEnumerator goRight() {
        tutorialText.text = "I appreciate your collaboration";
        for(int i=0; i<100; i++) {
            ship.GetComponent<Move>().steerLeft();
            yield return new WaitForSeconds(.02f);
        }
        tutorialStep = 2;
        StartCoroutine(showMeteorites());
    }

    public IEnumerator showMeteorites() {
        tutorialText.text = "Here're your mission:\nShoot'em all";
        for(float i=7f; i>=0; i-=.06f) {
            meteoriteParent.localPosition = new Vector2(0, i);
            yield return new WaitForSeconds(.01f);
        }
        ship.GetComponentInChildren<Weapon>().enabled = true;
        GameObject.Find("_GM").GetComponent<GameStates>().canPlay = true;
        tutorialStep = 3;
    }

    public IEnumerator meteoritesLearned() {
        tutorialText.text = "OK, I see that you get this";
        ship.GetComponentInChildren<Weapon>().enabled = false;
        GameObject.Find("_GM").GetComponent<GameStates>().canPlay = false;
        yield return new WaitForSeconds(3f);
        tutorialStep = 4;
        tutorialText.text = "Tap me when you're ready";
    }

	void Start () {
        meteoriteParent.position = new Vector2(100, 0);
        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();

        tutorialStep = 0;
        tutorialText.text = "Here's quick guide!\nTap there to go left!";
	}
	
	
	void FixedUpdate () {
        if(!pulseInProgress) 
        switch(tutorialStep) {
            case 0: StartCoroutine(pulse(left)); break;
            case 1: StartCoroutine(pulse(right)); break;
            case 3: 
                if(meteoriteParent.GetComponent<MeteoriteGenerator>().countAllMeteorites()[0] > 7) {
                    StartCoroutine(meteoritesLearned());
                }
                break;
        }
	}

    public void GoLeft() {
        if(tutorialStep == 0)
        StartCoroutine(goLeft());
    }

    public void GoRight() {
        if(tutorialStep == 1)
        StartCoroutine(goRight());
    }

    public void EndTutorial() {
        if(tutorialStep == 4) {
            PlayerPrefs.SetInt("tutorial_done", 1);
            PlayerPrefs.Save();

            GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("menu");
        }
    }

}
