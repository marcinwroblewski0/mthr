﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChallengesCounter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = (PlayerPrefs.GetInt("achievementsDone", 1) - 1) + "/38";
	}
}
