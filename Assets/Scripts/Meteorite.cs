using UnityEngine;
using System.Collections;

public class Meteorite : MonoBehaviour {

    public string owner = "~";
    private AchievementsManager am;
    private RecordManager rm;
    private new AudioSource audio;
    string player1Name, player2Name;
    Color p1color, p2color;

    void Start() {
        am = GameObject.Find("_GM").GetComponent<AchievementsManager>();
        rm = GameObject.Find("_GM").GetComponent<RecordManager>();
        audio = GetComponent<AudioSource>();

        player1Name = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getPlayerName();
        try {
            player2Name = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getPlayerName();
        } catch {
            player2Name = "AI";
            Debug.LogWarning("There's no second player!");
        }

        p1color = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getShipColor();
        try {
        p2color = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getShipColor();
        } catch {
            p2color = Ship.shipColors[0];
            Debug.LogWarning("There's no second player!");
        }
    }

    IEnumerator ownerChange() {
        for (int i = 0; i < 10; i++) {
            transform.localScale = new Vector2(
                transform.localScale.x + 0.02f,
                transform.localScale.y + 0.02f
                );

            yield return new WaitForSeconds(0.01f);
        }

        audio.pitch = Random.Range(0.8f, 1.3f);
        audio.Play();

        for (int i = 0; i < 10; i++){
            transform.localScale = new Vector2(
                transform.localScale.x - 0.02f,
                transform.localScale.y - 0.02f
                );

            yield return new WaitForSeconds(0.01f);
        }
    }

	public string getOwner(){
		return owner;
	}

	void OnTriggerEnter2D (Collider2D collider){

        if (collider.tag.Equals("Bullet")){
            Destroy(collider.gameObject);

            stealed(owner);

            gameObject.GetComponent<SpriteRenderer>().color =
                collider.GetComponent<SpriteRenderer>().color;

            StartCoroutine(ownerChange());

            owner = collider.GetComponent<Bullet>().getParentName();
        }
	}

	public void clear(){
		gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
		owner = "";
	}

	public void invert(){

		player1Name = GameObject.FindGameObjectWithTag ("Player1").GetComponent<Ship> ().getPlayerName ();
		player2Name = GameObject.FindGameObjectWithTag ("Player2").GetComponent<Ship> ().getPlayerName ();

		p1color = GameObject.FindGameObjectWithTag ("Player1").GetComponent<Ship> ().getShipColor ();
		p2color = GameObject.FindGameObjectWithTag ("Player2").GetComponent<Ship> ().getShipColor ();

		if (owner.Equals (player1Name)) {
			owner = player2Name;
			gameObject.GetComponent<SpriteRenderer> ().color = p2color;
		} else if (owner.Equals (player2Name)) {
			owner = player1Name;
			gameObject.GetComponent<SpriteRenderer> ().color = p1color;
		}
	}

    public void stealed(string from) {

        if (am != null) {

            if (from.Equals(player1Name)) {
                am.stolenFromP1toP2 += 1;
            } else if (from.Equals(player2Name)) {
                am.stolenFromP2toP1 += 1;
            }

        } else if (rm != null) {
            if (from.Equals(player2Name)) {
                rm.stolenFromP2toP1 += 1;
            }
        }
    }
}
