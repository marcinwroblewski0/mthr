﻿using UnityEngine;
using System.Collections;

public class Powerups : MonoBehaviour {

    IEnumerator destroy(){
        GetComponent<Collider2D>().enabled = false;

        for (int i = 0; i < 30; i++) {
            transform.localScale = new Vector2(i * 0.2f, i * 0.2f);
            Color currentColor = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f - (i * 0.02f));
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
    }

        void Start(){
		Destroy(gameObject, 4);
	}

	void OnTriggerEnter2D (Collider2D collider){

        string name = gameObject.name;

        Debug.Log(collider.name + " got " + name);



		if (collider.tag.Equals ("Player1") || collider.tag.Equals ("Player2")) {

            GameObject opponentShip = null;
            if (collider.tag.Equals("Player1"))         opponentShip = GameObject.FindGameObjectWithTag("Player2");
            else if (collider.tag.Equals("Player2"))    opponentShip = GameObject.FindGameObjectWithTag("Player1");

            if(!name.Contains("opponent")) {
			    if (name.Equals("fast(Clone)")) {
				    collider.GetComponent<Move> ().changeSpeedMultiplier (0.2f);
			    } else if (name.Equals("slow(Clone)")) {
				    collider.GetComponent<Move> ().changeSpeedMultiplier (-0.2f);
                } else if (name.Substring(0, 4).Equals("stop")) {
				    collider.GetComponent<Move> ().stop ();
			    } else if (name.Substring(0, 5).Equals("clear")) {
				    GameObject.Find ("MeteoriteParent").GetComponent<MeteoriteGenerator> ().clearAllMeteorites ();
			    } else if (name.Substring(0, 6).Equals("invert")) {
				    GameObject.Find ("MeteoriteParent").GetComponent<MeteoriteGenerator> ().invertAllMeteorites ();
			    } else if (name.Substring(0, 6).Equals("double")) {
                    collider.GetComponentInChildren<Weapon>().setMultiShoot(true, 2);
                } else if (name.Equals("tripleshoot(Clone)")) {
                    collider.GetComponentInChildren<Weapon>().setMultiShoot(true, 3);
                } else if (name.Substring(0, 6).Equals("quadra")) {
                    collider.GetComponentInChildren<Weapon>().setMultiShoot(true, 4);
                } else if (name.Substring(0, 5).Equals("penta")) {
                    collider.GetComponentInChildren<Weapon>().setMultiShoot(true, 5);
                } else if (name.Substring(0, 5).Equals("right")) {                                                                     /***/
                    GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>().setRightRotation(1);                       //
                } else if (name.Substring(0, 4).Equals("left")) {                                                                      //
                    GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>().setLeftRotation(1);                        //METEORITES
                } else if (name.Substring(0, 5).Equals("pause")) {                                                                     //
                    GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>().stopRotation();                           //
                } else if (name.Substring(0, 3).Equals("mix")) {                                                                      /***/
                    collider.GetComponent<Move>().invert();                          
                } else if (name.Substring(0, 2).Equals("up")) {                                                               
                    collider.GetComponentInChildren<Weapon>().FastShoot();                          
                } else if (name.Substring(0, 4).Equals("down")) {
                    collider.GetComponentInChildren<Weapon>().SlowShoot();
                }
            } else { 
                Debug.Log("For opponent");
                if (name.Substring(0, 8).Equals("freeze_o")) {
                    opponentShip.GetComponent<Move>().stop();
                } else if (name.Equals("slow_opponent(Clone)")) {
                    opponentShip.GetComponent<Move>().changeSpeedMultiplier(-0.2f);
                } else if (name.Equals("fast_opponent(Clone)")) {
                    opponentShip.GetComponent<Move>().changeSpeedMultiplier(0.2f);
                } else if(name.Substring(0, 11).Equals("fastshoot_o")) {
                    opponentShip.GetComponentInChildren<Weapon>().FastShoot();
                } else if(name.Substring(0, 11).Equals("slowshoot_o")) {
                    opponentShip.GetComponentInChildren<Weapon>().SlowShoot();
                } else if(name.Substring(0, 11).Equals("tripleshoot")) {
                    opponentShip.GetComponentInChildren<Weapon>().setMultiShoot(true, 3);
                }
            }

            collider.GetComponent<Ship>().powerupsUsed++;

            StartCoroutine(destroy());
		} else {
			Debug.LogWarning("Bullet: " + collider.name + ", " + collider.tag);
		}


	}
}
