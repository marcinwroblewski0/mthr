﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if(SceneManager.GetActiveScene().buildIndex != 0){
                GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("menu");
                //GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().hideBanner();
            } else {
				Application.Quit();
			}
		}
			
	}
}
