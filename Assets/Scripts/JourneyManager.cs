﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class JourneyManager : MonoBehaviour {

    public Text centerText;
    public Text playerResult, AIResult;
    public GameObject roundPanel;
    public Text winner;
    public GameObject VSPanel;
    public Text playerNameLabel, AINameLabel;


    private int meteoritesCount;
    private float SHOW_TIME = 2.5f;
    private int[] results;
    private RoundTimeManager rtm;
    private MeteoriteGenerator mg;
    private PowerupsManager pm;
    private GameStates gs;
    private Weapon p1Weapon, p2Weapon;
    private Ship p1Ship, p2Ship;
    public int galaxyId, level;
    public bool standardRules = true;
    public int stolenFromP2toP1, stolenFromP1toP2;
    private Move p1Move, p2Move;
    private long screenTouches;
    public string AIName, playerName;

    private bool isPanelAnimating = false;

    IEnumerator showVSPanel() {
        gs.setGameState(false);

        if(!isPanelAnimating) {

            isPanelAnimating = true;

            for (int i = 30; i >= 0; i--) {
                VSPanel.transform.position = new Vector2(i/2f, 0);
                yield return new WaitForSeconds(0.01f);
            }

            yield return new WaitForSeconds(SHOW_TIME);

            for (int i = 0; i <= 30; i++){
                VSPanel.transform.position = new Vector2(0, i/2f);
                yield return new WaitForSeconds(0.01f);
            }

            isPanelAnimating = false;
        }

        gs.setGameState(true);
    }

    IEnumerator startRoundWithOffset(bool won) {
        gs.setGameState(false);

        if(!isPanelAnimating) {

            isPanelAnimating = true;

            for (int i = 60; i >= 0; i--) {
                roundPanel.transform.position = new Vector2(i/3f, 0);
                yield return new WaitForSeconds(0.01f);
            }

            yield return new WaitForSeconds(SHOW_TIME);

            for (int i = 0; i <= 60; i++){
                roundPanel.transform.position = new Vector2(0, i/3f);
                yield return new WaitForSeconds(0.01f);
            }

            if(won) {
                GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeOutScene("storyteller");
            } else {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            isPanelAnimating = false;
        }

    }

    public int getMeteoritesCount() {
        return 30;
    }


    void Start () {
        rtm = GetComponent<RoundTimeManager>();
        mg = GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>();
        pm = GetComponent<PowerupsManager>();
        gs = GetComponent<GameStates>();
        p1Weapon = GameObject.FindGameObjectWithTag("Player1").GetComponentInChildren<Weapon>();
        p2Weapon = GameObject.FindGameObjectWithTag("Player2").GetComponentInChildren<Weapon>();
        p1Ship = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>();
        p2Ship = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>();
        p1Move = GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>();
        p2Move = GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>();

        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();

        playerName = p1Ship.getPlayerName();
        AIName = p2Ship.getPlayerName();

        standardRules = true;

        startNewRound();
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)){
            screenTouches++;
        }
    }

	void FixedUpdate () {
        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        if (isMollyWinning()) playerLost(AIName + " won");
        
        if (isLevelObjectiveDone(galaxyId, level)) playerWon();
    }

    public void startNewRound(){

        playerNameLabel.text = playerName;
        AINameLabel.text = AIName;

        screenTouches = 0;

        mg.rotateDirection = Random.Range(-2, 2);

        p1Weapon.bulletSpeed = 30f;
        p1Weapon.shootRate = 0.47f;
        p2Weapon.bulletSpeed = 30f;
        p2Weapon.shootRate = 0.47f;

        p1Move.turnovers = 0;
        p2Move.turnovers = 0;

        stolenFromP1toP2 = 0;
        stolenFromP2toP1 = 0;

        p1Weapon.resetBulletCount();
        p1Ship.powerupsUsed = 0;

        StartCoroutine(showVSPanel());

        mg.setupNewRound();
        rtm.SetTimeRules(30);

        galaxyId = PlayerPrefs.GetInt("currentGalaxy", 1);
        level = PlayerPrefs.GetInt("journeyLevel", 1);
        Debug.Log("Current galaxy: " + galaxyId + ", journey level: " + level);

        
        GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>().setStartPosition();
        GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>().setStartPosition();

        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        mg.setupNewRound();

        setRules(galaxyId, level);
        
    }



    void playerWon() {
        winner.color = Color.black;
        winner.text = playerName;
        StartCoroutine(startRoundWithOffset(true));

        Currency.Add(35);

        PlayerPrefs.SetInt("journeyLevel", level + 1);
        PlayerPrefs.Save();
    }

    public void playerLost(string why) {
        winner.color = Color.black;
        winner.text = AIName;
        GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().showFullscreenAd();

        StartCoroutine(startRoundWithOffset(false));

    }

    public void requestRoundEnd() {
        if(results[0] < results[1] && (float)results[1] / (float)meteoritesCount * 100f > 50) {
            playerLost("Opponent won");
        } else {
            playerWon();
        }
    }

    bool isLevelObjectiveDone(int galaxyId, int level) {
        switch(level) {
            case 18: return false;
            default: return isPlayerWinning();
        }
        
    }

    bool isMollyWinning() {
        return
            (results[0] < results[1] && (float)results[1] / (float)meteoritesCount * 100f > 50 && gs.getGameState())
            && standardRules;
    }

    bool isPlayerWinning() {
        return results[0] > results[1]  //czy gracz wygrywa                                          
        && (float)results[0] / (float)meteoritesCount * 100f > 50 //czy gracz wygrał
        && gs.getGameState(); //czy gra trwa
    }

    void setRules(int galaxyId, int level) {

        switch (level - 1) {
            case 0:
                standardRules = true;
                string[] allowed0 = {  };
                pm.setAllowed(allowed0);
                break;
            case 1:
                standardRules = true;
                string[] allowed1 = { PowerupsManager.CLEAR, PowerupsManager.INVERT };
                pm.setAllowed(allowed1);
                break;
            case 2:
                standardRules = true;
                string[] allowed2 = { PowerupsManager.METEORITES_LEFT, PowerupsManager.METEORITES_RIGHT, PowerupsManager.PAUSE };
                pm.setAllowed(allowed2);
                break;
            case 3:
                standardRules = true;
                string[] allowed3 = { PowerupsManager.FAST, PowerupsManager.SLOW, PowerupsManager.STOP, PowerupsManager.MIX };
                pm.setAllowed(allowed3);
                break;
            case 4:
                standardRules = true;
                string[] allowed4 = { PowerupsManager.SLOWSHOOT, PowerupsManager.FASTSHOOT, PowerupsManager.TRIPLESHOOT, PowerupsManager.QUADRASHOOT, PowerupsManager.PENTASHOOT };
                pm.setAllowed(allowed4);
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                break;
            case 10:
            case 11:
            case 12:
                standardRules = true;
                string[] allowed13 = { PowerupsManager.SLOWSHOOT, PowerupsManager.FASTSHOOT, PowerupsManager.TRIPLESHOOT, PowerupsManager.QUADRASHOOT,
                    PowerupsManager.PENTASHOOT, PowerupsManager.CLEAR, PowerupsManager.FAST, PowerupsManager.SLOW, PowerupsManager.MIX, PowerupsManager.PAUSE,
                    PowerupsManager.STOP, PowerupsManager.METEORITES_LEFT, PowerupsManager.METEORITES_RIGHT};
                pm.setAllowed(allowed13);
                p2Ship.disableCollider();
                break;
            case 13:
            case 14:
            case 15:
                p2Ship.disableCollider();
                standardRules = true;
                break; 
            default:
                standardRules = true;
                break;
        }
    }
}
