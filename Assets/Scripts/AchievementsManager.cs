﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class AchievementsManager : MonoBehaviour {

    public Text objectiveText, objectiveStatus, objectiveResult;
    public Text ResultText;
    public GameObject roundPanel;
    private int meteoritesCount;
    private float SHOW_TIME = 2.5f;
    private int[] results;
    private RoundTimeManager rtm;
    private MeteoriteGenerator mg;
    private PowerupsManager pm;
    private GameStates gs;
    private Weapon p1Weapon, p2Weapon;
    private Ship p1Ship;
    public int achievementId;
    public bool standardRules = true;
    public int stolenFromP2toP1, stolenFromP1toP2;
    private Move p1Move, p2Move;
    private int screenTouches;
    private bool currencyAdded = false;

    private bool isPanelAnimating = false;

    IEnumerator showPanel() {
        gs.setGameState(false);

        if(!isPanelAnimating) {

            isPanelAnimating = true;

            for (int i = 30; i >= 0; i--) {
                roundPanel.transform.position = new Vector2(i/2f, 0);
                yield return new WaitForSeconds(0.01f);
            }

            yield return new WaitForSeconds(SHOW_TIME);

            for (int i = 0; i <= 30; i++){
                roundPanel.transform.position = new Vector2(0, i/2f);
                yield return new WaitForSeconds(0.01f);
            }

            isPanelAnimating = false;
        }

        gs.setGameState(true);
    }

    IEnumerator startRoundWithOffset() {
        gs.setGameState(false);

        if(!isPanelAnimating) {

            isPanelAnimating = true;

            for (int i = 30; i >= 0; i--) {
                roundPanel.transform.position = new Vector2(i/2f, 0);
                yield return new WaitForSeconds(0.01f);
            }

            yield return new WaitForSeconds(SHOW_TIME);

            for (int i = 0; i <= 30; i++){
                roundPanel.transform.position = new Vector2(0, i/2f);
                yield return new WaitForSeconds(0.01f);
            }

            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
            isPanelAnimating = false;
        }

    }

    public int getMeteoritesCount() {
        return 30;
    }


    void Start () {
        PlayerPrefs.SetString("BotName", "Molly");
        PlayerPrefs.Save();

        rtm = GetComponent<RoundTimeManager>();
        mg = GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>();
        pm = GetComponent<PowerupsManager>();
        gs = GetComponent<GameStates>();
        p1Weapon = GameObject.FindGameObjectWithTag("Player1").GetComponentInChildren<Weapon>();
        p2Weapon = GameObject.FindGameObjectWithTag("Player2").GetComponentInChildren<Weapon>();
        p1Ship = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>();
        p1Move = GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>();
        p2Move = GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>();


        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();

        standardRules = true;

        startNewRound();
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)){
            screenTouches++;
        }
    }

	void FixedUpdate () {
        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        if (isAchievementDone(achievementId)) playerWon();

        if (isMollyWinning()) playerLost("Molly won");
    }

    public void startNewRound(){
        screenTouches = 0;

        mg.rotateDirection = Random.Range(-2, 2);

        p1Weapon.bulletSpeed = 30f;
        p1Weapon.shootRate = 0.47f;
        p1Weapon.bulletSpeed = 30f;
        p1Weapon.shootRate = 0.47f;

        p1Move.turnovers = 0;
        p2Move.turnovers = 0;

        stolenFromP1toP2 = 0;
        stolenFromP2toP1 = 0;

        p1Weapon.resetBulletCount();
        p1Ship.powerupsUsed = 0;

        StartCoroutine(showPanel());

        mg.setupNewRound();
        rtm.SetTimeRules(30);

        achievementId = PlayerPrefs.GetInt("achievementsDone", 1);

        LookForAchievements();

        Debug.Log("Current acheivement: " + achievementId);

        objectiveText.text = getRule(achievementId);
        objectiveResult.text = "Good luck ♥";
        objectiveResult.fontStyle = FontStyle.Normal;
        objectiveResult.color = Color.black;
        GameObject.FindGameObjectWithTag("Player1").GetComponent<Move>().setStartPosition();
        GameObject.FindGameObjectWithTag("Player2").GetComponent<Move>().setStartPosition();

        results = mg.countAllMeteorites();
        meteoritesCount = results[0] + results[1] + results[2];

        mg.setupNewRound();

        setRules(achievementId);
        
    }

    void LookForAchievements() {
        if(achievementId >= 12) {
            GPGAchievements.ReportAchievement(GPGAchievements.WARM_UP);
            Currency.Add(20);
        }
    }

    void playerWon() {
        objectiveResult.color = Color.green;
        objectiveResult.text = "Success";
        if(!currencyAdded) {
            Currency.Add(25 * (achievementId/12 + 1));
            currencyAdded = true;
        }

        StartCoroutine(startRoundWithOffset());

        PlayerPrefs.SetInt("achievementsDone", achievementId + 1);
        PlayerPrefs.Save();

        GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().showFullscreenAd();
    }

    public void playerLost(string why) {
        objectiveResult.color = Color.red;
        objectiveResult.text = why;

        Currency.Add(CurrencyConverter.For(
            (int)(Time.time - rtm.startTime), 
            screenTouches, 
            p1Weapon.getBulletCount(), 
            (int)p1Move.turnovers,
            p1Ship.powerupsUsed, 
            stolenFromP2toP1));

        StartCoroutine(startRoundWithOffset());
    }

    bool isAchievementDone(int achievementId) {
        switch (achievementId) {
            case 0:
            case 1:
                return isPlayerWinning();
            case 2:
                int bulletsLeft = 100 - p1Weapon.getBulletCount();
                objectiveStatus.text = bulletsLeft.ToString();
                if (bulletsLeft <= 0) playerLost("Out of bullets");
                return isPlayerWinning() && bulletsLeft >= 0;
            case 3:
                int powerupsNeeded = 3 - p1Ship.getPowerupsUsed();
                objectiveStatus.text = powerupsNeeded.ToString();
                return powerupsNeeded <= 0;
            case 4:
                objectiveStatus.text = (30 - stolenFromP2toP1).ToString();
                return stolenFromP2toP1 >= 30;
            case 5:
                objectiveStatus.text = (5 - results[0]).ToString();
                if (results[0] > 5) playerLost("You're too greedy");
                return results[0] <= 5 && rtm.timeLeft <= 0.1f;
            case 6:
                objectiveStatus.text = (5 - p1Move.turnovers).ToString();
                return p1Move.turnovers >= 5;
            case 7:
                objectiveStatus.text = (10 - results[1]).ToString();
                if (results[1] > 10) playerLost("Molly won");
                return results[1] <= 10 && rtm.timeLeft <= 0.1f;
            case 8:
                objectiveStatus.text = "☻";
                if (p1Move.turnovers > 0) playerLost("I warned you");
                return p1Move.turnovers < 1 && rtm.timeLeft <= 0.1f;
            case 9:
                objectiveStatus.text = (results[0] - results[1]).ToString();
                if (results[0] - results[1] < 0) playerLost("Molly won");
                return results[0] - results[1] > 0 && rtm.timeLeft <= 0.1f;
            case 10:
                objectiveStatus.text = (50 - screenTouches).ToString();
                return screenTouches >= 50;
            case 11:
                objectiveStatus.text = p1Move.getSpeedMultiplier().ToString();
                return p1Move.getSpeedMultiplier() > 1.5f;
            case 12:
                objectiveStatus.text = Mathf.Abs(mg.rotateDirection).ToString();
                return Mathf.Abs(mg.rotateDirection) >= 3;
            case 13:
                return isPlayerWinning();
            case 14:
                bulletsLeft = 50 - p1Weapon.getBulletCount();
                objectiveStatus.text = bulletsLeft.ToString();
                if (bulletsLeft <= 0) playerLost("Out of bullets");
                return isPlayerWinning() && bulletsLeft >= 0;
            case 15:
                powerupsNeeded = 5 - p1Ship.getPowerupsUsed();
                objectiveStatus.text = powerupsNeeded.ToString();
                return powerupsNeeded <= 0;
            case 16:
                objectiveStatus.text = (45 - stolenFromP2toP1).ToString();
                return stolenFromP2toP1 >= 45;
            case 17:
                objectiveStatus.text = (3 - results[0]).ToString();
                if (results[0] > 3) playerLost("You're too greedy");
                return results[0] <= 3 && rtm.timeLeft <= 0.1f;
            case 18:
                objectiveStatus.text = (7 - p1Move.turnovers).ToString();
                return p1Move.turnovers >= 7;
            case 19:
                objectiveStatus.text = (7 - results[1]).ToString();
                if (results[1] > 7) playerLost("Molly won");
                return results[1] <= 7 && rtm.timeLeft <= 0.1f;
            case 20:
                objectiveStatus.text = "☻";
                if (p1Move.turnovers > 0) playerLost("I warned you");
                return p1Move.turnovers < 1 && rtm.timeLeft <= 0.1f;
            case 21:
                objectiveStatus.text = (results[0] - results[1]).ToString();
                if (results[0] - results[1] < 0) playerLost("Molly won");
                return results[0] - results[1] > 0 && rtm.timeLeft <= 0.1f;
            case 22:
                objectiveStatus.text = (100 - screenTouches).ToString();
                return screenTouches >= 100;
            case 23:
                objectiveStatus.text = p1Move.getSpeedMultiplier().ToString();
                return p1Move.getSpeedMultiplier() > 2f;
            case 24:
                objectiveStatus.text = Mathf.Abs(mg.rotateDirection).ToString();
                return Mathf.Abs(mg.rotateDirection) >= 5;
            case 25:
                return isPlayerWinning();
            case 26:
                bulletsLeft = 30 - p1Weapon.getBulletCount();
                objectiveStatus.text = bulletsLeft.ToString();
                if (bulletsLeft <= 0) playerLost("Out of bullets");
                return isPlayerWinning() && bulletsLeft >= 0;
            case 27:
                powerupsNeeded = 7 - p1Ship.getPowerupsUsed();
                objectiveStatus.text = powerupsNeeded.ToString();
                return powerupsNeeded <= 0;
            case 28:
                objectiveStatus.text = (60 - stolenFromP2toP1).ToString();
                return stolenFromP2toP1 >= 60;
            case 29:
                objectiveStatus.text = (2 - results[0]).ToString();
                if (results[0] > 2) playerLost("You're too greedy");
                return results[0] <= 2 && rtm.timeLeft <= 0.1f;
            case 30:
                objectiveStatus.text = (10 - p1Move.turnovers).ToString();
                return p1Move.turnovers >= 10;
            case 31:
                objectiveStatus.text = (6 - results[1]).ToString();
                if (results[1] > 6) playerLost("Molly won");
                return results[1] <= 6 && rtm.timeLeft <= 0.1f;
            case 32:
                objectiveStatus.text = "☻";
                if (p1Move.turnovers > 0) playerLost("I warned you");
                return p1Move.turnovers < 1 && rtm.timeLeft <= 0.1f;
            case 33:
                objectiveStatus.text = (results[0] - results[1]).ToString();
                if (results[0] - results[1] < 0) playerLost("Molly won");
                return results[0] - results[1] > 0 && rtm.timeLeft <= 0.1f;
            case 34:
                objectiveStatus.text = (130 - screenTouches).ToString();
                return screenTouches >= 130;
            case 35:
                objectiveStatus.text = p1Move.getSpeedMultiplier().ToString();
                return p1Move.getSpeedMultiplier() >= 2.6f;
            case 36:
                objectiveStatus.text = Mathf.Abs(mg.rotateDirection).ToString();
                return Mathf.Abs(mg.rotateDirection) >= 7;
            case 37:
                objectiveStatus.text = (1 - p1Ship.getPowerupsUsed()).ToString();
                if (p1Ship.getPowerupsUsed() > 0) playerLost("Don't touch powerups!");
                return p1Ship.getPowerupsUsed() <= 0 && rtm.timeLeft <= 0.1f;
            case 38:
                objectiveStatus.text = (15 - p1Ship.collidedWithOpponent).ToString();
                return (p1Ship.collidedWithOpponent >= 15);

            default: return false;
        }
    }

    bool isMollyWinning() {
        return
            (results[0] < results[1] && (float)results[1] / (float)meteoritesCount * 100f > 50 && gs.getGameState())
            && standardRules;
    }

    bool isPlayerWinning() {
        return results[0] > results[1]  //czy gracz wygrywa                                          
        && (float)results[0] / (float)meteoritesCount * 100f > 50 //czy gracz wygrał
        && gs.getGameState(); //czy gra trwa
    }

    void setRules(int achievementId) {
 
        switch (achievementId) {
            case 0:
            case 1:
                rtm.SetTimeRules(30);
                break;
            case 2: break;
            case 3: break;
            case 4:
                standardRules = false;
                break;
            case 5:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 6: break;
            case 7:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 8:
                rtm.SetTimeRules(32.5f);
                break;
            case 9:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                p2Weapon.shootRate = 0.6f;
                p2Weapon.bulletSpeed = 20f;
                break;
            case 10:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 11:
                string[] allowedFor11 = { PowerupsManager.FAST, PowerupsManager.SLOW };
                pm.setAllowed(allowedFor11);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 4f;
                break;
            case 12:
                mg.stopRotation();
                string[] allowedFor12 = { PowerupsManager.METEORITES_RIGHT, PowerupsManager.METEORITES_LEFT };
                pm.setAllowed(allowedFor12);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 4f;
                break;
            case 13:
                rtm.SetTimeRules(17.5f);
                break;
            case 14: break;
            case 15:
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 4f;
                break;
            case 16:
                standardRules = false;
                break;
            case 17:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 18: break;
            case 19:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 20:
                rtm.SetTimeRules(32.5f);
                break;
            case 21:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                p2Weapon.shootRate = 0.6f;
                p2Weapon.bulletSpeed = 20f;
                break;
            case 22:
                standardRules = false;
                rtm.SetTimeRules(22.5f);
                break;
            case 23:
                string[] allowedFor23 = {  PowerupsManager.SLOW, PowerupsManager.FAST };
                pm.setAllowed(allowedFor23);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 3f;
                break;
            case 24:
                mg.stopRotation();
                string[] allowedFor24 = { PowerupsManager.METEORITES_RIGHT, PowerupsManager.METEORITES_LEFT };
                pm.setAllowed(allowedFor24);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 3f;
                break;
            case 25:
                rtm.SetTimeRules(12.5f);
                break;
            case 26: break;
            case 27:
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 4f;
                break;
            case 28:
                standardRules = false;
                break;
            case 29:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 30:
                rtm.SetTimeRules(37.5f);
                break;
            case 31:
                float[] movePattern31 = { 0.5f, 0.5f, 0.2f, 0.9f, 1f, 1f, 0.5f, 0.5f, 0.7f, 0.3f, 0.3f, 0.3f, 0.3f, 0.1f, 0.1f, 0.9f, 1f, 1f, 2f, 2f, 4f, 4f, 5f };
                p2Move.setMovePattern(movePattern31);
                string[] allowedFor31 = { };
                pm.setAllowed(allowedFor31);
                mg.stopRotation();
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                break;
            case 32:
                rtm.SetTimeRules(47.5f);
                break;
            case 33:
                standardRules = false;
                rtm.SetTimeRules(17.5f);
                p2Weapon.shootRate = 0.5f;
                p2Weapon.bulletSpeed = 30f;
                break;
            case 34:
                standardRules = false;
                rtm.SetTimeRules(32.5f);
                break;
            case 35:
                string[] allowedFor35 = { PowerupsManager.FAST, PowerupsManager.SLOW };
                pm.setAllowed(allowedFor35);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 3f;
                pm.spawnEvenly = true;
                rtm.SetTimeRules(47.5f);
                break;
            case 36:
                mg.stopRotation();
                string[] allowedFor36 = { PowerupsManager.METEORITES_RIGHT, PowerupsManager.METEORITES_LEFT };
                pm.setAllowed(allowedFor36);
                pm.minSpawnTime = 0.5f;
                pm.maxSpawnTime = 3f;
                rtm.SetTimeRules(47.5f);
                break;
            case 37:
                string[] allowedFor37 = { PowerupsManager.FAST, PowerupsManager.SLOW, PowerupsManager.METEORITES_LEFT, PowerupsManager.METEORITES_RIGHT,
                PowerupsManager.PAUSE, PowerupsManager.MIX, PowerupsManager.STOP};
                pm.setAllowed(allowedFor37);
                pm.minSpawnTime = 3f;
                pm.maxSpawnTime = 3.1f;
                rtm.SetTimeRules(32.5f);
                float[] movePattern37 = { 0.5f, 2f, 0.2f, 0.9f, 0.5f, 1f, 0.5f, 1f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.1f, 0.1f, 0.9f, 1f, 1f, 2f, 2f, 4f, 4f, 5f };
                p2Move.setMovePattern(movePattern37);
                break;
            case 38:
                pm.setAllowed(new string[] { PowerupsManager.METEORITES_LEFT, PowerupsManager.METEORITES_RIGHT, PowerupsManager.TRIPLESHOOT});
                rtm.SetTimeRules(32.5f);
                p2Move.setMovePattern(new float[] { 1f, 0.3f, 0.8f, 2f, 4f, 1f, 1f, 1f, 0.3f, 0.1f, 0.5f, 4f, 4f});
                break;
            default:
                standardRules = true;
                break;
        }
    }

    string getRule(int achievementId) {
        switch (achievementId) {
            case 0:
            case 1:
                return "Defeat Molly in 30 seconds";
            case 2:
                return "Defeat Molly with less than 100 bullets";
            case 3:
                return "Get 3 powerups";
            case 4:
                return "Steal 30 meteorites from Molly";
            case 5:
                return "Own at most 5 meteorites";
            case 6:
                return "Do 5 turnovers";
            case 7:
                return "Molly can own at most 10 meteorites";
            case 8:
                return "Don't go to the Molly's side";
            case 9:
                return "Keep advantage";
            case 10:
                return "Touch the screen 50 times";
            case 11:
                return "Accelerate!";
            case 12:
                return "Accelerate the meteorites!";
            case 13:
                return "Defeat Molly in 15 seconds";
            case 14:
                return "Defeat Molly with less than 50 bullets";
            case 15:
                return "Get 5 powerups";
            case 16:
                return "Steal 45 meteorites from Molly";
            case 17:
                return "Own at most 3 meteorites";
            case 18:
                return "Do 7 turnovers";
            case 19:
                return "Molly can own at most 7 meteorites";
            case 20:
                return "Don't go to the Molly's side";
            case 21:
                return "Keep advantage";
            case 22:
                return "Touch the screen 100 times";
            case 23:
                return "Accelerate to 2";
            case 24:
                return "Accelerate the meteorites to 5!";
            case 25:
                return "Defeat Molly in 10 seconds";
            case 26:
                return "Defeat Molly with less than 30 bullets";
            case 27:
                return "Get 7 powerups";
            case 28:
                return "Steal 60 meteorites from Molly";
            case 29:
                return "Own at most 2 meteorites";
            case 30:
                return "Do 10 turnovers";
            case 31:
                return "Molly can own at most 6 meteorites";
            case 32:
                return "Don't go to the Molly's side";
            case 33:
                return "Keep advantage";
            case 34:
                return "Touch the screen 130 times";
            case 35:
                return "Accelerate to 2.6";
            case 36:
                return "Accelerate the meteorites to 7!";
            case 37:
                return "Won without powerups";
            case 38:
                return "Touch Molly 15 times";

            default: return "There're no more challenges. At least now.";
            
        }
    }
}
