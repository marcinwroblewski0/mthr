﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour {

	public Text winnerText, loserText, winnerNameText, loserNameText, wPoints, lPoints;
    public Image VS;
    public Sprite[] fightPoses;
	public Text ResultText;
	public GameObject roundPanel;
	private string winner, loser;
	private int p1Rounds, p2Rounds, meteoritesCount;
	private float SHOW_TIME = 2.5f;
	private int[] results;
	private bool isRoundPanelVisible, showed;
	private float timeToShow;
    private RoundTimeManager rtm;
    private MeteoriteGenerator mg;

	void Start(){
		winner = "N";
		loser = "N";
		p1Rounds = p2Rounds = 0;
        rtm = GetComponent<RoundTimeManager>();
        mg = GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>();

        GameObject.Find("TransitionMask").GetComponent<TransitionMask>().fadeInScene();

        results = mg.countAllMeteorites ();
		meteoritesCount = results [0] + results [1] + results [2];

        VS.sprite = fightPoses[Random.Range(0, fightPoses.Length)];

        showStartGamePanel();
	}

	void FixedUpdate () {

		
		results = mg.countAllMeteorites ();
        meteoritesCount = results[0] + results[1] + results[2];

		ResultText.text = p1Rounds + " : " + p2Rounds;

		if (isRoundPanelVisible && !showed) {
			showed = true;
			timeToShow = Time.time + SHOW_TIME;
		}

		if (timeToShow < Time.time) {
			hideRoundPanel();
		}

        if ((results[0] / (float)meteoritesCount) * 100f > 50f){
            p1Rounds++;

            Debug.Log("Winner is " + winner + ": " + results[0] + ", loser is " + loser + ": " + results[1]);
            startNewRound();
        } else if ((results[1] / (float)meteoritesCount) * 100f > 50f){
            p2Rounds++;

            Debug.Log("Winner is " + winner + ": " + results[1] + ", loser is " + loser + ": " + results[0]);
            startNewRound();
        }
	
	}

	public string getWinnerName(){
		return winner;
	}

	public string getLoserName(){
		return loser;
	}

    public int getCurrentRound(){
        return p1Rounds + p2Rounds + 1;
    }

	public void startNewRound(){
		showRoundPanel ();
		
		GameObject.FindGameObjectWithTag ("Player1").GetComponent<Move> ().setStartPosition ();
		GameObject.FindGameObjectWithTag ("Player2").GetComponent<Move> ().setStartPosition ();

		results = mg.countAllMeteorites ();
		meteoritesCount = results [0] + results [1] + results [2];

        GameObject.Find("MeteoriteParent").GetComponent<MeteoriteGenerator>().setupNewRound();

        rtm.SetTimeRules(15f);
    }

	private void showRoundPanel(){
        Debug.Log(results[0] + " + " + results[1] + " + " + results[2] + " = " + meteoritesCount);

        int winnerResult, loserResult;

        if (results[0] > results[1]) {
            winner = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getPlayerName();
            loser = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getPlayerName();
            winnerResult = results[0];
            loserResult = results[1];
        } else {
            winner = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getPlayerName();
            loser = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getPlayerName();
            winnerResult = results[1];
            loserResult = results[0];
        }

        Color winnerColor = GameObject.Find(winner).GetComponent<Ship>().getShipColor();
        Color loserColor = GameObject.Find(loser).GetComponent<Ship>().getShipColor();

        winnerText.text = "Winner";
		loserText.text = "Loser";
        VS.enabled = false;

		winnerNameText.text = winner;
		winnerNameText.color = winnerColor;
			
		loserNameText.text = loser;
		loserNameText.color = loserColor;

		wPoints.text = string.Format("{0:N2}", (float)winnerResult / (float)meteoritesCount * 100f) + "%";
		wPoints.color = winnerColor;
			
		lPoints.text = string.Format("{0:N2}", (float)loserResult / (float)meteoritesCount * 100f) + "%";
		lPoints.color = loserColor;
	
		roundPanel.transform.position = new Vector2 (0, 0);
		isRoundPanelVisible = true;
		GameObject.Find ("_GM").GetComponent<GameStates> ().setGameState (false);
	}

    private void showStartGamePanel() {
        winnerText.text = "";
        loserText.text = "";
        VS.enabled = true;

        winnerNameText.text = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getPlayerName();
        winnerNameText.color = GameObject.FindGameObjectWithTag("Player1").GetComponent<Ship>().getShipColor();

        loserNameText.text = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getPlayerName();
        loserNameText.color = GameObject.FindGameObjectWithTag("Player2").GetComponent<Ship>().getShipColor();

        roundPanel.transform.position = new Vector2(0, 0);
        isRoundPanelVisible = true;
        GameObject.Find("_GM").GetComponent<GameStates>().setGameState(false);
    }

	private void hideRoundPanel(){
		roundPanel.transform.position = new Vector2 (500, 0);
		isRoundPanelVisible = false;
		showed = false;
		GameObject.Find ("_GM").GetComponent<GameStates> ().setGameState (true);
    }

    public void requestRoundEnd() {
        if (results[0] > results[1]) p1Rounds++;
        else p2Rounds++;

        GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().showFullscreenAd();

        startNewRound();
    }
}
