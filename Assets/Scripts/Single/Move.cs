using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public KeyCode left;
	public KeyCode right;
	public int playerSide;
	public float distanceFromCenter;
	private float speedMultiplier;
	public GameObject center;

	private float movingSide;
	private float moveTime;
	private int AIMoveSide;
	
	private float stopTime;

    private int invertMove;
    private float invertTime;

    public float turnovers;
    private bool halfTurnover;
    public bool isMovePatternSet;

    private float[] movePattern;
    private int patternStep;
    private float startTime;

    public float lastMoveTime;

    private GameStates gs;

    void Start () {
		setStartPosition ();

		moveTime = AIMoveSide = 0;
        invertMove = 1;
        turnovers = 0f;
        patternStep = 0;
        movePattern = new float[0];
        startTime = Time.time;

        gs = GameObject.Find("_GM").GetComponent<GameStates>();
	}
	

	void Update() {
		if (gs.getGameState() == true && !isStopped()) {
			playerSteer ();
        }

        isInverted();

        Vector2 position = new Vector2(
				Mathf.Cos(movingSide) * distanceFromCenter,
				Mathf.Sin(movingSide) * distanceFromCenter);
			
		transform.position = position;

			
		Vector3 diff = center.transform.position - transform.position;
		diff.Normalize();
			
		float rotZ = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 90);

        if (!halfTurnover && transform.position.x < -3.5f) {
            halfTurnover = true;
        }

        if (halfTurnover && transform.position.x > 3.5f ) {
            halfTurnover = false;
            turnovers += 1f;
        }

        if (patternStep >= movePattern.Length) {
            isMovePatternSet = false;
        }
    }


	public void setStartPosition(){

			Vector2 position = new Vector2(0, 0);
			
			if (playerSide == 0) {/*0 to lewa strona*/
				position = new Vector2 (-4f, 0);
			} else if (playerSide == 1) {
				position = new Vector2 (4f, 0);
			} else if (playerSide == -1) {
				position = new Vector2 (4f, 0);
			}
			
			transform.position = position;
			speedMultiplier = 1f;
			distanceFromCenter = position.x;
		}

	private void playerSteer(){
		if (!(playerSide >= 0)) {
			steerAI();
			if (AIMoveSide > 1) {
				steerRight ();
			} else {
				steerLeft();
            }
            lastMoveTime = Time.time;	
		} else {

            if (Input.GetKey (left)) {
				steerLeft ();
                lastMoveTime = Time.time;
			} else if (Input.GetKey (right)) {
				steerRight ();
                lastMoveTime = Time.time;
			}
				
			for (int i=0; i<Input.touchCount; i++) {
				Touch touch = Input.GetTouch(i);

                lastMoveTime = Time.time;

				if(playerSide == 0){
					if(touch.position.x < (Screen.width/2) && touch.position.y < Screen.height/2){
						steerLeft();
					}else if(touch.position.x < (Screen.width/2) && touch.position.y > Screen.height/2){
						steerRight();
					}
						
				}else if(playerSide == 1){
					if(touch.position.x > (Screen.width/2) && touch.position.y > Screen.height/2){
						steerLeft();
					}else if(touch.position.x > (Screen.width/2) && touch.position.y < Screen.height/2){
						steerRight();
					}
				}
			}
		}
	}


	private void steerAI(){

		if ((AIMoveSide == 0 || moveTime < Time.time)) {
            if (isMovePatternSet) {
                moveTime = Time.time + movePattern[patternStep];
			    AIMoveSide = (patternStep%2) + 1;
                patternStep++;
                Debug.Log("Pattern step: " + patternStep + ", timed: " + (Time.time - startTime));
			    
            } else {
                
                float inertness = 0.4f / PlayerPrefs.GetFloat("difficulty", 1f);
                float adroitness = 2.4f / PlayerPrefs.GetFloat("difficulty", 1f);
                moveTime = Time.time + Random.Range (inertness, adroitness);
			    AIMoveSide = Random.Range(0, 3);
            }
		} 
	}

	public void steerRight(){
		movingSide -= (2.2f * speedMultiplier * Time.deltaTime) * invertMove;
	}

	public void steerLeft(){
		movingSide += (2.2f * speedMultiplier * Time.deltaTime) * invertMove;
    }

	public void changeSpeedMultiplier(float value){
        
        speedMultiplier += value;
        Debug.Log(gameObject.name + " have " + speedMultiplier + " speed");
        GetComponent<Ship>().showTrail((speedMultiplier > 1f));
	}

	public float getSpeedMultiplier(){
		return speedMultiplier;
	}

    public void invert() {
        invertTime = Time.time + 2.5f;
    }

	public void stop(){
		stopTime = Time.time + 2.5f;
	}

	public bool isStopped(){
		if (Time.time > stopTime) {
			return false;
		} else {
			return true;
		}
	}

    public bool isInverted() {
        if (Time.time > invertTime) {
            invertMove = 1;
			return false;
		} else {
            invertMove = -1;
            return true;
		}
    }

    public void setMovePattern(float[] pattern) {
        movePattern = pattern;
        isMovePatternSet = true;
    }
}
