﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PowerupsManager : MonoBehaviour {

	public Transform[] powerups;

	private float distanceFromCenter;
	private float cooldownTime;
    private Transform[] allowedPowerups;

    public bool spawnEvenly = false;
    private int powerupsSpawned;

    public float minSpawnTime = 1f;
    public float maxSpawnTime = 8f;

    public static string CLEAR = "clear";
    public static string SLOWSHOOT = "down";
    public static string FAST = "fast";
    public static string INVERT = "invert";
    public static string METEORITES_LEFT = "left";
    public static string MIX = "mix";
    public static string PAUSE = "pause";
    public static string PENTASHOOT = "tripleshoot";    //były za mocne 
    public static string QUADRASHOOT = "tripleshoot";   //
    public static string METEORITES_RIGHT = "right";
    public static string SLOW = "slow";
    public static string STOP = "stop";
    public static string TRIPLESHOOT = "tripleshoot";
    public static string FASTSHOOT = "up";
    public static string FREEZE_OPPONENT = "freeze_opponent";

    private int powerupCounter;

    IEnumerator spawnPowerup(int powerupId) {
        Transform powerup = Instantiate(
            allowedPowerups[powerupId]) as Transform;


        float pos = 1.5f*(powerupCounter%4);
        powerupCounter++;
        powerup.position = new Vector2(
            Mathf.Cos(pos) * distanceFromCenter,
            Mathf.Sin(pos) * distanceFromCenter);

        powerup.transform.localScale = new Vector3(0, 0, 0);

        for (int i = 0; i < 20; i++) {
            powerup.localScale = new Vector2(
                    powerup.transform.localScale.x + 0.05f,
                    powerup.transform.localScale.y + 0.05f
                );
            if (i < 19) powerup.Rotate(new Vector3(0, 0, powerup.localRotation.z + 18f));

            yield return new WaitForSeconds(0.01f);
        }

        powerup.GetComponent<BoxCollider2D>().enabled = true;
        powerupsSpawned++;
    }

	void Start () {
		distanceFromCenter = 3.5f;
		cooldownTime = Random.Range (minSpawnTime, maxSpawnTime) + Time.time;
        allowedPowerups = powerups;
	}
	
	
	void FixedUpdate () {
		if (cooldownTime < Time.time && SceneManager.GetActiveScene().buildIndex != 0 && allowedPowerups.Length > 0) {
            if(!spawnEvenly) {
			    StartCoroutine(spawnPowerup(Random.Range(0, allowedPowerups.Length)));
            } else {
                StartCoroutine(spawnPowerup(powerupsSpawned%allowedPowerups.Length));
            }
			cooldownTime = Random.Range (minSpawnTime, maxSpawnTime) + Time.time;
		}
	}

    public void setAllowed(string[] powerupsIds) {
        allowedPowerups = new Transform[powerupsIds.Length];

        Debug.Log("Can use " + allowedPowerups.Length + " of " + powerups.Length + " powerups");

        for (int i = 0; i < powerups.Length; i++) {
            for (int j = 0; j < allowedPowerups.Length; j++) {
                if (powerups[i].name.Contains(powerupsIds[j])) {
                    allowedPowerups[j] = powerups[i];
                    Debug.Log("allowed powerup #" + j + " is " + allowedPowerups[j].name);
                }
            }
        }
    }
}
