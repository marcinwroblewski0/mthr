﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public GameObject bulletPrefab;

    int bulletCount;

	public float shootRate;
	public float bulletSpeed;
	private float shootCooldown;
	private bool canShoot;

    private bool canMultishoot;
    public float multiShootDuration;
    private int bulletsInMultiShoot;

    public int getBulletCount() {
        return bulletCount;
    }

    public void resetBulletCount() {
        bulletCount = 0;
    }

    public void setShooting(bool approval){
		canShoot = approval;
	}

    public void setMultiShoot(bool approval, int bulletAtOnce) {
        canMultishoot = approval;
        if (approval){
            multiShootDuration = Time.time + 3f;
            bulletsInMultiShoot = bulletAtOnce;
        }
    }
    
	void Start () {
        multiShootDuration = Time.time + 3f;
		shootCooldown = 0f;
        if (bulletSpeed == 0) {
		    bulletSpeed = 30f;
		    shootRate = 0.47f;
        }
	}
	
	void Update () {
        if(Time.time - GetComponentInParent<Move>().lastMoveTime < 3f) {
		    if (Time.time > shootCooldown && !canMultishoot) {
                Shoot ();
		    }else if (Time.time > shootCooldown && canMultishoot){
                MultiShoot(bulletsInMultiShoot);
            }
        }
        if (Time.time > multiShootDuration) {
            setMultiShoot(false, 1);
        }

        if (canShoot != GameObject.Find ("_GM").GetComponent<GameStates> ().getGameState ())
			setShooting (GameObject.Find ("_GM").GetComponent<GameStates> ().getGameState ());
	}

	public void Shoot(){
		if (canShoot) {
			shootCooldown = shootRate + Time.time;

			GameObject shotObject = Instantiate(bulletPrefab) as GameObject;
			shotObject.GetComponent<SpriteRenderer>().color = 
				gameObject.GetComponentInParent<SpriteRenderer>().color;

			shotObject.GetComponent<Bullet>().setParentName(gameObject.transform.parent.name);
			shotObject.transform.position = transform.position;

			Vector3 diff = Vector3.zero - shotObject.transform.position;

			shotObject.GetComponent<Rigidbody2D>().AddRelativeForce(diff * bulletSpeed);

            bulletCount++;
		}
	}

    public void MultiShoot(int bulletsAtOnce) {
        if (canShoot){
            shootCooldown = shootRate + Time.time;
            for (int i = (int)-Mathf.Ceil(bulletsAtOnce/2); i <= Mathf.Ceil(bulletsAtOnce / 2); i++) {
                GameObject shotObject = Instantiate(bulletPrefab) as GameObject;
                shotObject.GetComponent<SpriteRenderer>().color =
                    gameObject.GetComponentInParent<SpriteRenderer>().color;

                int directionX = i;
                int directionY = i;

                if (transform.position.x > 0) directionX *= -1;
                if (transform.position.y > 0) directionY *= -1;

                shotObject.GetComponent<Bullet>().setParentName(gameObject.transform.parent.name);
                shotObject.transform.position = transform.position + new Vector3(0.3f * directionX, 0.3f * directionY, 0);
                Vector3 diff = Vector3.zero - (shotObject.transform.position - new Vector3(1f * directionX, 1f * directionY, 0f));

                shotObject.GetComponent<Rigidbody2D>().AddRelativeForce(diff * bulletSpeed);

                bulletCount++;
            }
        }
    }

    public void FastShoot() {
        Debug.Log("FastShoot");
        shootRate -= 0.1f;
    }

    public void SlowShoot() {
        Debug.Log("SlowShoot");
        shootRate += 0.1f;
    }
}
