﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Skins : MonoBehaviour {

    public Sprite dflt;
    public Sprite camo;
    public Sprite puzl;
    public Sprite hnmb;
    public Sprite fade;


    public static int CurrentSkin() {
        return PlayerPrefs.GetInt("currentSkin", 0);
    }

    public static int BoughtSkins() {
        return PlayerPrefs.GetInt("boughtSkins", 0);
    }

    public static void SetCurrentSkin(int id) {
        PlayerPrefs.SetInt("currentSkin", id);
        PlayerPrefs.Save();
    }

    public static void SetBoughtSkins(int id) {
        PlayerPrefs.SetInt("boughtSkins", id);
    }

    public Sprite CurrentSkinSprite() {
        switch(CurrentSkin()) {
            case 1:
                return camo;
            case 2:
                return puzl;
            case 3:
                return hnmb;
            case 4:
                return fade;
            default:
                return dflt;
        }
    }
}
