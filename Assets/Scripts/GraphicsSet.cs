﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GraphicsSet : MonoBehaviour {

	void Start(){
		if (PlayerPrefs.GetInt ("graphics", 1) == 0) {
			gameObject.GetComponent<Toggle> ().isOn = true;
		} else {
			gameObject.GetComponent<Toggle> ().isOn = false;
		}
	}

	public void setGraphics(){
		if (gameObject.GetComponent<Toggle> ().isOn) {
			PlayerPrefs.SetInt ("graphics", 0);
		} else {
			PlayerPrefs.SetInt ("graphics", 1);
		}
	}
}
