﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SingleSummary : MonoBehaviour {

    public Text bestTouches, bestStolen, bestTurnovers, bestPowerups, bestBullets, bestTime;
    public Text lastTouches, lastStolen, lastTurnovers, lastPowerups, lastBullets, lastTime;
    public Text labelTouches, labelStolen, labelTurnovers, labelPowerups, labelBullets, labelTime;

	// Use this for initialization
	void Start () {
        GameObject.Find("_audio(Clone)").GetComponent<AdsManager>().showFullscreenAd();

        Currency.Add(CurrencyConverter.For(
            (int)PlayerPrefs.GetFloat("lastSingleTime"),
            PlayerPrefs.GetInt("lastSingleTouches"),
            PlayerPrefs.GetInt("lastSingleBullets"),
            (int)PlayerPrefs.GetFloat("lastSingleTurnovers"),
            PlayerPrefs.GetInt("lastSinglePowerups"),
            PlayerPrefs.GetInt("lastSingleStolen")));

        CompareResults();
        FillTable();
        LookForAchievements();
	}


    void CompareResults() {
        if(PlayerPrefs.GetInt("bestSingleTouches", 0) < PlayerPrefs.GetInt("lastSingleTouches")) {
            labelTouches.color = Color.green;
            PlayerPrefs.SetInt("bestSingleTouches", PlayerPrefs.GetInt("lastSingleTouches")); 
        }

        if(PlayerPrefs.GetInt("bestSingleStolen", 0) < PlayerPrefs.GetInt("lastSingleStolen")) {
            labelStolen.color = Color.green;
            PlayerPrefs.SetInt("bestSingleStolen", PlayerPrefs.GetInt("lastSingleStolen"));
        }

        if(PlayerPrefs.GetFloat("bestSingleTurnovers", 0) < PlayerPrefs.GetFloat("lastSingleTurnovers")) {
            labelTurnovers.color = Color.green;
            PlayerPrefs.SetFloat("bestSingleTurnovers", PlayerPrefs.GetFloat("lastSingleTurnovers"));
        }

        if(PlayerPrefs.GetInt("bestSinglePowerups", 0) < PlayerPrefs.GetInt("lastSinglePowerups")) {
            labelPowerups.color = Color.green;
            PlayerPrefs.SetInt("bestSinglePowerups", PlayerPrefs.GetInt("lastSinglePowerups"));
        }

        if(PlayerPrefs.GetInt("bestSingleBullets", 0) < PlayerPrefs.GetInt("lastSingleBullets")) {
            labelBullets.color = Color.green;
            PlayerPrefs.SetInt("bestSingleBullets", PlayerPrefs.GetInt("lastSingleBullets"));
        }

        if(PlayerPrefs.GetFloat("bestSingleTime", 0) < PlayerPrefs.GetFloat("lastSingleTime")) {
            labelTime.color = Color.green;
            PlayerPrefs.SetFloat("bestSingleTime", PlayerPrefs.GetFloat("lastSingleTime"));
        }
    }

    void FillTable() {
        bestTouches.text = PlayerPrefs.GetInt("bestSingleTouches", PlayerPrefs.GetInt("lastSingleTouches")).ToString();
        bestStolen.text = PlayerPrefs.GetInt("bestSingleStolen", PlayerPrefs.GetInt("lastSingleStolen")).ToString();
        bestTurnovers.text = PlayerPrefs.GetFloat("bestSingleTurnovers", PlayerPrefs.GetFloat("lastSingleTurnovers")).ToString("0.0");
        bestPowerups.text = PlayerPrefs.GetInt("bestSinglePowerups", PlayerPrefs.GetInt("lastSinglePowerups")).ToString();
        bestBullets.text = PlayerPrefs.GetInt("bestSingleBullets", PlayerPrefs.GetInt("lastSingleBullets")).ToString();
        bestTime.text = PlayerPrefs.GetFloat("bestSingleTime", PlayerPrefs.GetFloat("lastSingleTime")).ToString("0.00");

        lastTouches.text = PlayerPrefs.GetInt("lastSingleTouches").ToString();
        lastStolen.text = PlayerPrefs.GetInt("lastSingleStolen").ToString();
        lastTurnovers.text = PlayerPrefs.GetFloat("lastSingleTurnovers").ToString("0.0");
        lastPowerups.text = PlayerPrefs.GetInt("lastSinglePowerups").ToString();
        lastBullets.text = PlayerPrefs.GetInt("lastSingleBullets").ToString();
        lastTime.text = PlayerPrefs.GetFloat("lastSingleTime").ToString("0.00");

        PlayerPrefs.Save();
    }

    void LookForAchievements() {
        if(PlayerPrefs.GetFloat("bestSingleTime", 0) > 30f) {
            GPGAchievements.ReportAchievement(GPGAchievements.ON_ONES_METTLE);
        }
    }
}
