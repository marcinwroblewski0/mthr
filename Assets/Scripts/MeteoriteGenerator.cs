using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MeteoriteGenerator : MonoBehaviour {

    public int meteoritesCount = 1;
	public Transform meteoritePrefab;

	private Transform[] allMeteorites;
	string P01Name;
	string P02Name;
    private PointsManager pm;
    private AchievementsManager am;
    private TutorialManager tm;

    public float rotateDirection;

	// Use this for initialization
	void Start () {
        setupNewRound();

        P01Name = GameObject.FindGameObjectWithTag("Player1").name;
        try {
		    P02Name = GameObject.FindGameObjectWithTag("Player2").name;
        } catch {
            Debug.LogWarning("There's no second player!");
            P02Name = "AI";
        }
        rotateDirection = Random.Range(-2, 2);

        if (Mathf.Abs(rotateDirection) < .5f) rotateDirection += 1f;

        GameObject gm = GameObject.Find("_GM");
        pm = gm.GetComponent<PointsManager>();
        am = gm.GetComponent<AchievementsManager>();
        tm = gm.GetComponent<TutorialManager>();

        Debug.Log ("P1: " + P01Name + ", P2: " + P02Name);
	}


	void FixedUpdate(){
		if (GameObject.Find ("_GM").GetComponent<GameStates> ().getGameState () == true) {
			transform.Rotate(0, 0, rotateDirection );
		}
	}

    public void setLeftRotation(float value) {
        rotateDirection += value;
    }

    public void setRightRotation(float value){
        rotateDirection -= value;
    }

    public void stopRotation() {
        rotateDirection = 0;
    }

    private void getAllMeteorites(){
		allMeteorites = new Transform[transform.childCount];

		for (int i=0; i<allMeteorites.Length; i++) {
			allMeteorites[i] = gameObject.transform.GetChild(i);
		}
	}

    public void destroyAllMeteorites(){
        for (int i = 0; i < allMeteorites.Length; i++){
            Destroy(allMeteorites[i].gameObject);
        }
    }

	public void clearAllMeteorites(){
		for (int i=0; i<allMeteorites.Length; i++) {
			allMeteorites [i].GetComponent<Meteorite> ().clear ();
		}
	}

	public void invertAllMeteorites(){
		for (int i=0; i<allMeteorites.Length; i++) {
			allMeteorites [i].GetComponent<Meteorite> ().invert ();
		}
	} 

    public void createMeteorites(){


        
        if (pm != null) {
            meteoritesCount = 30 - (pm.getCurrentRound() * 2);
        } else {
            meteoritesCount = 30;
        }

        if (meteoritesCount <= 0) meteoritesCount = 1;

       
        for (int i = 0; i < meteoritesCount; i++){
            Transform meteorite = Instantiate(meteoritePrefab) as Transform;
            meteorite.transform.name = "meteorite" + i;
            meteorite.SetParent(transform);
            meteorite.transform.position = new Vector2(
                2.5f * Mathf.Cos(((Mathf.PI * 2) * i) / meteoritesCount),
                2.5f * Mathf.Sin(((Mathf.PI * 2) * i / meteoritesCount)));
        }

        Debug.Log("Created " + meteoritesCount + " meteorites");
            
    }

    public void setupNewRound(){
        if(allMeteorites != null) destroyAllMeteorites();
        if (!SceneManager.GetActiveScene().name.Equals("tutorial"))
        createMeteorites();

        getAllMeteorites();
    }

	public int[] countAllMeteorites(){
		if (allMeteorites != null) {
            getAllMeteorites();
            string[] results = new string[allMeteorites.Length];

            for (int i = 0; i < allMeteorites.Length; i++){
                results[i] = allMeteorites[i].GetComponent<Meteorite>().getOwner();
            }
			
				

			int P01, P02, clear;
			P01 = P02 = clear = 0;

			for (int i = 0; i<results.Length; i++) {
				if (results [i].Equals (P01Name)) {
					P01++;
				} else if (results [i].Equals (P02Name)) {
					P02++;
				} else {
					clear++;
				}
			}

			return new int[]{P01, P02, clear};
		} else {
			return new int[3];
		}
	}
}
