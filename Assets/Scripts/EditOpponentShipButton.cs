﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EditOpponentShipButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    if (PlayerPrefs.GetString("nick2", "").Equals("")) {
            Destroy(gameObject);
        }
	}
	
	public void ConfigureOpponentShip() {
        SceneManager.LoadScene(5);
    }
}
