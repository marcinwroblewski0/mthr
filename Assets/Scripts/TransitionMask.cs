﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TransitionMask : MonoBehaviour {

    IEnumerator fadeOut() {
        Image mask = GameObject.Find("TransitionMask").GetComponent<Image>();

        for (int j = 0; j <= 255; j+=10) {
            mask.color = new Color(0, 0, 0, j/255f);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator fadeOut(string toScene) {
        Image mask = GameObject.Find("TransitionMask").GetComponent<Image>();

        for (int j = 0; j <= 255; j+=10) {
            mask.color = new Color(0, 0, 0, j/255f);
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene(toScene);
    }

    IEnumerator fadeIn() {
        Image mask = GameObject.Find("TransitionMask").GetComponent<Image>();

        for (int j = 255; j >= 0; j-=10) {
            mask.color = new Color(0, 0, 0, j/255f);
            yield return new WaitForEndOfFrame();
        }
    }

	public void fadeOutScene() {
        StartCoroutine(fadeOut());
    }

    public void fadeOutScene(string toScene) {
        StartCoroutine(fadeOut(toScene));
    }

    public void fadeInScene() {
        StartCoroutine(fadeIn());
    }
}
